define([
 
    'core/application',
    'core/components/base_list',
    'collections/notifications',
    'components/notifications/views/modal/item',
    'text!components/notifications/tpls/modal/list.html',

], function( Application, BaseListView , Collection, ItemView, MainTpl ) {
    
        var NotificationsModalListView = BaseListView.extend({
            className   : '',
            name        : 'notifications',
            hasTable    : false,
            
            initialize: function () {
                this.collection = new Collection([],{
                    filter : {
                        status : 0
                    }
                }); 
                
                this.hasTable = false;
                this.collection.on('remove', this.renderItems, this);
                NotificationsModalListView.__super__.initialize.call(this, this.options);

                var $this = this;
                var notifyTime = setInterval(function(){
                    $this.collection.reset();
                    $this.collection.setFilters({
                        status : 0
                    });
                    $this.collection.fetch();
                }, 60000);
            },

            render: function(){

                var tpl = this.getTemplate();
                $('li.notification-holder').append(tpl(_.extend(this.options, {
                    Application     : Application
                })));

                this.afterRender();
                return this;
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },

            beforeRenderItems: function(resp, collection) {
                $('ul.menuBig').empty();
                if(this.collection.length){
                    var count = 0;
                    _.each(this.collection.toJSON(), function(item){
                        if( item.status == 0 ){
                            count ++;
                        }
                    });

                    $('.count-info .badge-primary').html(count).removeClass('hide');

                }else{

                    $('.count-info .badge-primary').addClass("hide")
                }
            },

            renderItem: function(model) {

                this.itemView = new ItemView({model: model, collection: this.collection});
                $('ul.menuBig').append( this.itemView.el );             
            },

            afterRenderItems: function() {

                var tpl = '<li><div class="text-center link-block"> <a href="#notifications"> <strong>مشاهده تمام اعلان ها</strong> <i class="fa fa-angle-left"></i> </a> </div></li>';
                $('ul.menuBig').append( tpl );
            },

            
        });  
        return NotificationsModalListView; 
});