define([

    'core/application',
    'core/components/base_section',
    'collections/notifications',
    'components/notifications/views/page/list',
    'text!components/notifications/tpls/page/main.html',
    
    ], function(Application, BaseView, Collection, ListView, MainTpl) {
        var MainView = BaseView.extend({
            name            : 'notifications',
            // currentRoute    : 'suggestion',
            fetchAsset      : false,
            fetchProvince   : false,
            
            initialize: function(options) {
                
                if( this.options && this.options.currentRoute ){
                    this.currentRoute = this.options.currentRoute;
                }

                if( this.currentRoute == "notification/suggestion" ){

                    this.collection = new Collection();  
                    this.collection.filters.set({ type : 3 });

                }else if( this.currentRoute == "notification/order" ){
                    
                    this.collection = new Collection();
                    this.collection.filters.set({ type : 7 });   
                }
                
                MainView.__super__.initialize.call(this, this.options);   
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },
            
            getListView: function(){
                return new ListView({
                        collection : this.collection,
                    });
            },

        });

        return MainView;
    });

