define([
    
    'core/application',
    'core/components/base_list',    
    'collections/news',
    'components/news/views/form',
    'text!components/news/tpls/list.html',

], function( Application, BaseListView, NewsCollection, FormView, ListTpl) {
        
        var NewsListView = BaseListView.extend({
            typeView    : 'row',
            className   : "news-list",            
            events:{
                'click .creat-item .btn'       : 'creatItem',
            },
            
            initialize: function(options) {
                _.bindAll(this,'loadMore');

                this.status = this.options.status;
                this.mode = this.options.mode;

                this.collection = new NewsCollection([], {
                    filter : {
                        status : this.status
                    }
                });                

                NewsListView.__super__.initialize.call(this, this.options);
                return this;
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            getTableOptions: function(){
                var aoColumns = '', columnDefs = '';

                columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns = this.getClumens();

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },            
            getClumens: function () {
                var list = [];

                list   = 
                [   
                    { "mDataProp":  'id' },
                    { "mDataProp":  'title' },
                    { "mDataProp":  'sambols' },
                    { "mDataProp":  'source' },
                    { "mDataProp":  'publishDate' },
                    { "mDataProp":  'action' },
                ];
                
                return list;
            },

            itemValue: function(model){

                // var arr = [];
                // _.each( model.get('sambols') , function(item){  
                //     arr.push( '<span class="nemad">' + item + '</span>' );
                // })

                var obj = {
                    'id'            : '<div class = "left-text">' + ( model.get('id') ? model.get('id') : '' ) + '</div>',
                    'title'         : '<span class="whiteNorm">' + ( model.get('title') ? model.get('title') : '' ) + '</span>',
                    'sambols'       : '<span class="whiteNorm"><span class="nemad">'  + ( model.get('sambols') ? model.get('sambols') : '' ) +  '</span></span>',                    
                    'source'        : '<span class="whiteNorm">' + ( model.get('source') ? model.get('source') : '' ) + '</span>',
                    'publishDate'   : '<div class="date-value ltr">' + Application.convertUTCDate( model.get('publishDate'), 'YYYY/MM/DD H:MM' ) + '</div>',
                    'action'        : '<a class = "show-page center" href = "#news/'+ ( model.get('id') ? model.get('id') : '') +'"><span class="text-state-info">اطلاعات بیشتر</span></a>',
                };

                return obj;
            },

            creatItem: function(e){
                this.formView = new FormView({
                    collection: this.collection,
                    mode : 'create',
                });
                this.formView.render();
                this.formView.afterAppend(); 
            },

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    Application     : Application,
                    mode            : this.mode,
                })));

                this.afterRender();
                return this;
            },
        });
        return NewsListView;
    });
