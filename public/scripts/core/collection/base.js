define([
    'jquery',
    'underscore',
    'backbone',
    'core/model/base',
    'moment'

    ], function ($, _, Backbone, BaseModel) {
        var BaseCollections = Backbone.Collection.extend({

            initialize: function(models, options){

                this.options = options || {};
                this.params = this.options.params || {};
                this.filter = this.options.filter || {};
                this.filters = new BaseModel();

                this.setParams();
                this.setFilters();

                this.on("reset", function(){
                    this.filters.clear();
                    this.setParams();
                    this.setFilters();
                }, this);
            },
            setParams : function(){
                this.filters.set(_.extend({},{
                    limit : 0,
                    offset : 0
                }, this.params));
            },
            setFilters: function(obj) {
                var obj = obj || {};

                this.filters.set({
                    filter : _.extend({}, this.filter, obj)
                });
            },
            parse: function( resp ){
                this.totalCount = resp.count;
                return resp.list || resp;
            },

            setLimitOffsetFilters: function(limit, offset) {

                if( limit ){
                    this.filters.set({limit: limit});
                }
                if( offset ){
                    this.filters.set({offset: offset});
                }else{
                    this.filters.set({offset: 0});
                }
            },

            unsetFilter: function(key) {

                // var filter = this.filters.toJSON();
                // delete filter[key];
                // this.filters.set(filter);
                this.filters.unset(key);
            },

            unsetFilters: function(obj) {

                var filter = this.filters.toJSON();
                delete filter;
                this.filters.clear();
            },

            increase: function( count ){
                this.filters.set('offset', this.filters.get('offset') + this.filters.get('limit'));
                return this;
            },

            getOptions: function(){
                return this.options;
            },

            fetch: function(options){

                this.lastFetchTime = moment().format("YYYY/MM/DD hh:mm:ss");

                options || (options = {});

                this.trigger("beforeFetch")
                var collection = this,
                    success = options.success;

                if (this.fetchXhr && this.fetchXhr.readyState > 0) {
                    this.fetchXhr.abort();
                }

                $.extend(options, {
                    update: true,
                    remove: false,
                    silent: true
                });

                if(this.filters){
                    var filters = this.filters.toJSON();
                    if(options.data){
                        for( var i in filters ){
                            options.data[i] = filters[i];
                        }
                    }
                    else{
                        options.data = filters;
                    }
                }

                options.beforeSend = function(xhr) {
                    collection.fetchXhr = xhr;
                    //xhr.setRequestHeader('Authorization', 'Bearer 001bed113f3dd46831562d0b359a28152a86c447'); // admin
                    // xhr.setRequestHeader('Authorization', 'Bearer 8fb0acf50ce09b1e35fa1d8369924e0e73a712f3'); // operator
                    // xhr.setRequestHeader('Authorization', 'Bearer 8640fbe8eb4c75a017cf1cd4257e0bc14b9d7f5d');    // execution
                    // xhr.setRequestHeader('Authorization', 'Bearer 8928695d01516f7a9ad67952c6084311907e79fd');    // contractors
                    
                    collection.fetchXhr = xhr;
                };

                options.success = function( collection, resp, xhr){
                    collection.isFetched = true;
                    collection.trigger('fetchSuccess', resp, collection);
                    if (success) success(collection, resp);

                }

                return Backbone.Collection.prototype.fetch.call(this, options);
            },

            makeParams: function(fields, value){
                var string = [];
                var count = 0;
                var first = true;

                for (var i = 0; i < fields.length; i++) {
                    if ( fields[i] && value[i] > 0 ) {
                        string.push(fields[i] + '=' + value[i]);
                        count += 1;
                    }
                }

                if (string.length === 0) {
                    return '';
                }

                return (first ? '?' : '&') + string.join('&');
            }

        });

        return BaseCollections;
    });
