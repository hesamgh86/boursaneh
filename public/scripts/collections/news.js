define([
    
    'core/application',
    'core/collection/base',
    'models/news'

], function( Application, BaseCollections, NewsModel ) {

    var Collection = BaseCollections.extend({
        model: NewsModel,
        name : "News",
        url : function(){
            return Application.URL + '/api/posts';
        },
    });

    return Collection;
});
