define([
    'jquery',
    'backbone',
    'underscore',
    'core/application',
    'core/components/base_view',
    
], function($, Backbone, _, Application, BaseView ) {
	var MapBaseView = BaseView.extend({
		zoom          : 15,
		mapHolderName : 'map-canvas',
		viewType      : 'view',  // create, edit, view
        styles        : [],
        position      : {},
        defaultCenterPosition:{
            latitude: 32.7528674, // center of iran
            longitude: 53.4149349,
        },
        markerIcons   : {
            "1" : '/images/pin.png',
        },

        initialize: function() {
        	this.setInitOpt();
        },

        setInitOpt: function(){

            if( this.options && this.options.position ){
                this.position = this.options.position;
                this.defaultCenterPosition = this.options.position;
            }

            if( this.options && this.options.mapHolderName ){
                this.mapHolderName = this.options.mapHolderName;
            }

            if( this.options && this.options.viewType ){
                this.viewType = this.options.viewType;
            }
        },

        render: function(){

            this.afterRender();
            return this;
        },

        afterRender: function(){
           
            var self = this,
                mapInterval = setInterval(function(){
                if (typeof google != 'undefined') {
                    self.loadMap();
                    clearInterval(mapInterval);
                }
            }, 500);
        },

        loadMap: function(){
            
            this.map = new google.maps.Map(document.getElementById(this.mapHolderName), this.getMapOptions());
            
            if( this.viewType == 'create' ){
                this.setCover(1);
            
            }else if( this.viewType == 'edit' ){
                this.addEventMarker();
                var position = new google.maps.LatLng( this.position.latitude , this.position.longitude );
                if( this.position.latitude && this.position.longitude){
                    this.addMarker( position );
                }else{
                    this.setCover(1);
                }
                
            }else if( this.viewType == 'view' ){
                var position = new google.maps.LatLng( this.position.latitude , this.position.longitude );
                if( this.position.latitude && this.position.longitude){
                    this.addMarker( position );
                }else{
                    this.setCover(2);
                }
            }

            Application.AppEvent.trigger('map:is:loaded', this.map);
        },

        getMapOptions: function(){

            return {

                zoom        : this.zoom,
                center      : new google.maps.LatLng( this.defaultCenterPosition.latitude, this.defaultCenterPosition.longitude ),
                styles      : [
                    {
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [{ "color": "#444444" }]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{ "color": "#f2f2f2" }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{ "color": "#596a80" },{ "visibility": "on" }]
                    }
                ]
            };
        },

        setCover: function(type){

            if( type == 1 ){
                $('#'+this.mapHolderName).after('<div class="mask-map"><span class="select-map">مکان مورد نظر را انتخاب کنید</span></div>');
            }

            if( type == 2 ){
                if( this.viewType == 'view' ){
                    $('#'+this.mapHolderName).append('<div class="mask-map-2"><span class="select-map-2">آدرس روی نقشه مشخص نشده</span></div>');
                }else{
                    $('#'+this.mapHolderName).after('<div class="mask-map-2"><span class="select-map">آدرس روی نقشه مشخص نشده</span></div>');
                }
            }
            
            $( ".select-map" ).click(function() {
                $('.mask-map').remove();
            });        
        },

        addEventMarker: function(){

        	var self = this;
        	this.map.addListener('click', function(e) {
                
                var position = e.latLng;
                
                if( self.marker ){
                    self.marker.setPosition( position );
                
                }else{
                    self.addMarker( position );
                }
            });
        },

        addMarker: function(position){

        	this.marker = new google.maps.Marker({
                position    : position,
                map         : this.map,
                icon        : this.markerIcons[1],
            });
        },

        getPosition: function(){
            
            if( this.marker ){
                var position = this.marker.getPosition();

                return {
                    latitude  : position.lat(),
                    longitude : position.lng()
                }
            }
            return false;
        },
	});

	return MapBaseView;
});