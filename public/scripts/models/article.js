define([

    'core/application',
    'core/model/base',

], function( Application, BaseModel ) {
    var Model = BaseModel.extend({
        name : "Articles",
        itemValidation: {
            content:{
                required: true,
            },
            categoryId:{
            	required: true
            }
        },
       
        url : function(){
            return Application.URL + "/api/articles" + (this.get("id") ? "/" + this.get("id") : "") + (this.get("dateStr") ? "?dateStr=" + this.get("dateStr") : "");
        },
    });
    return Model;
});
