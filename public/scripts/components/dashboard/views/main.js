define([

    'core/application',
    'core/components/base_section',
    'text!components/dashboard/tpls/main.html',
    
    ], function(Application, BaseSection, MainTpl) {
        var MainView = BaseSection.extend({
            name        : 'dashboard',
            events:{
                'click .accordionTitle'                                          : 'toggleAccordion',
            },

            initialize: function(options) {
                MainView.__super__.initialize.call(this, this.options);
            },
            
            beforeRender: function(){
                this.render();
            },
            afterRender: function(){

            },
            getTemplate: function(){
                return _.template(MainTpl);
            },

            createListView: function(listHolder,list , txt , num){

                this.listHolder = listHolder;
                this.listView = this.getListView(list , txt , num);

                this.$(this.listHolder).html(this.listView.$el);
                this.listView.afterAppend();
                this.bindingScripts();
            },            
            
            getListView: function(list , txt , num){
                return new list({
                    txt    : txt,
                    num    : num,
                    mode   : 'dashboard'
                });
            },

            afterAppend: function(){},
            
            toggleAccordion: function (e) {

                var el = $(e.currentTarget),
                    id = el.attr('aria-controls');
                
                if( el.hasClass('title-is-expanded') ){
                    el.removeClass('title-is-expanded');
                }else{
                    $('.title-is-expanded').removeClass('title-is-expanded')
                    el.addClass('title-is-expanded');
                }

                if( !$('#'+id).hasClass('is-expanded') ){
                    $('.is-expanded').addClass('is-collapsed').removeClass('is-expanded');
                }

                if( $('#'+ id).hasClass('is-collapsed') ){
                    $('#'+ id).removeClass('is-collapsed').addClass('is-expanded');
                }else{
                    $('#'+ id).removeClass('is-expanded').addClass('is-collapsed');
                }
            }

        });

    return MainView;
});