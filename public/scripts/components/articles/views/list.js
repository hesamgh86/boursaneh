define([
    
    'core/application',
    'core/components/base_list',    
    'collections/articles',  
    'models/article',
    'text!components/articles/tpls/list.html',

    /* plugins */
    'moment',
    'persiandate',

], function( Application, BaseListView, Collection, Model, ListTpl, moment, PersianDate) {
        
        var TaskListView = BaseListView.extend({
            typeView    : 'row',
            className   : "articles-list",          
         
            events:{
                'click .btn-search'  : 'searchItems'
            },
            
            initialize: function(options) {
                _.bindAll(this,'searchItems');

                this.status = this.options.status;
                this.mode = this.options.mode;
                this.collection = new Collection();

                TaskListView.__super__.initialize.call(this, this.options);
                return this;
            },

            beforeRender: function(){
                
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            getTableOptions: function(){
                var aoColumns = '', columnDefs = '';

                columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns = this.getClumens();

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },            

            getClumens: function () {
                
               var list = [   
                    { "mDataProp":  'day' },
                    { "mDataProp":  'date' },
                    { "mDataProp":  'action', "width": "80px"},
                ];
                

                return list;
            },

            itemValue: function(model){
                
                var date = '', dateStr = '';
                if( model.get('date') ){
                    dateStr = moment( model.get('date') ).format('YYYY-MM-DD');
                }
                
                if( model.get('date') ){
                    var d = new persianDate( model.get('date') );
                    date = d.pDate.date + ' ' + new persianDate( model.get('date') ).format('MMMM') + ' ' + d.pDate.year;
                }

                var obj = {
                    'day'    : new persianDate( model.get('date') ).format('dddd') ,
                    'date'   : date,
                    'action' : '<a class = "show-page" href = "#articles/'+ dateStr +'"><span class="text-state-info">اطلاعات بیشتر</span></a>',
                };

                return obj;
            },

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    Application     : Application,
                    mode            : this.mode,
                })));
                this.afterRender();
                return this;
            },

            searchItems: function(){
               
                var start = this.$('.start-date').val();
                var end = this.$('.end-date').val();
                if( start && end ){

                    var data = {
                        startDate : this.changePersianToMiladi(start),
                        endDate   : this.changePersianToMiladi(end),
                    }

                    if(  moment( data.startDate  ).isAfter( data.endDate  )  ){
                        this.$('.start-date').closest('.form-control').addClass('has-error');
                        this.$('.end-date').closest('.form-control').addClass('has-error');
                        return;
                    }  

                    this.search(data);
                }
            },

            changePersianToMiladi: function(value){

                var array = value.split('/');
                var d = persianDate([parseInt(array[0]), parseInt(array[1]), parseInt(array[2])]).gDate;
                return moment(d).unix() * 1000;
            },

            search: function(data){

                var self = this;
                this.collection.reset();

                $.ajax({
                    url     : Application.URL + '/api/articles/find',
                    data    : JSON.stringify( data ),
                    type    : 'POST',
                    contentType: "application/json",
                    success: function (list, status){
                        // successCallback && successCallback(object, status);
                        var l = [];
                        _.each(list, function(item){
                            l.push({
                                id: item,
                                date: item,
                            });
                        })
                        self.collection.add(l);
                        $('.tbl-article').removeClass('hide');
                    },
                    error: function(m,e){

                    }
                })
            },
        });
        return TaskListView;
    });
