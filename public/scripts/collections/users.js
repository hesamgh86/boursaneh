define([
    
    'core/application',
    'core/collection/base',
    'models/user'

], function( Application, BaseCollections, UserModel ) {

    var Collection = BaseCollections.extend({
        model: UserModel,
        name : "Users",
        url : function(){
            return Application.URL + '/api/users';
        },
        setParams : function(){
            this.filters.set(_.extend({}, this.params));
        },
    });

    return Collection;
});
