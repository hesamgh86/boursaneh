define([
    'core/application',
    'core/model/base',
    ], function( Application, BaseModel ) {
        var Model = BaseModel.extend({
            url : function(){
                return this.baseUrl +'/api/assets';
            },
        });
        return Model;
    });
