define([

    'core/application',
    'core/collection/base',
    'models/asset'

], function( Application, BaseCollections, AssetModel ) {

    var Collection = BaseCollections.extend({
        
        model: AssetModel,      
        url : function(){           
            return '/api/assets';
        },
    });

    return Collection;
});
