define([

    'core/application',
    'core/components/base_form',  
    'models/user',
    'text!components/users/tpls/form.html',

    ], function( Application, BaseFormView, Model, FormTemplate ){
        
        var FormView = BaseFormView.extend({
            className       : 'modal-dialog modal-lg',
            name            : 'location',
            mode            : 'create',
            markerChangeByProvincePosition: true,

            events: function(){

                var parentEvents = BaseFormView.prototype.events;
                if(_.isFunction(parentEvents)){
                      parentEvents = parentEvents();
                }
                return _.extend({},parentEvents,{
                    
                });
            },
            
            initialize: function(options) {
                if(this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if(this.options && this.options.modalHolder ){
                    this.modalHolder = this.options.modalHolder;
                }

                
                FormView.__super__.initialize.call(this, this.options);
            },

            getTemplate: function(){

                return _.template(FormTemplate);
            },

            afterAppend: function(){
                BaseFormView.__super__.afterAppend.call(this, this.options);
 
            },


                                      
        });

        return FormView;
    });
