define([

    'core/application',
    'core/components/base_form',  
    'models/branche',
    'text!components/branches/tpls/form.html',

    ], function( Application, BaseFormView, Model, FormTemplate ){
        
        var FormView = BaseFormView.extend({
            className       : 'modal-dialog modal-lg',
            name            : 'location',
            mode            : 'create',
            isNeedMap       : true,
            mapHolderName   : 'map-canvas-form',
            markerChangeByProvincePosition: true,
            participationHide : false,

            events: function(){

                var parentEvents = BaseFormView.prototype.events;
                if(_.isFunction(parentEvents)){
                      parentEvents = parentEvents();
                }
                return _.extend({},parentEvents,{

                    'change select.participationType' : 'onChangeParticipationType',
                });
            },
            
            initialize: function(options) {
                
                if(this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if(this.options && this.options.modalHolder ){
                    this.modalHolder = this.options.modalHolder;
                }

                
                FormView.__super__.initialize.call(this, this.options);
            },

            getTemplate: function(){

                return _.template(FormTemplate);
            },
            afterAppend: function(){   
               if( this.isNeedMap ){
                    this.setMapInit();
                }             
                BaseFormView.__super__.afterAppend.call(this, this.options);

                if( !this.model.get('location') ){
                    $('.mask-map').removeClass('hide');
                    $( ".select-map" ).click(function() {
                        $('.mask-map').remove();
                    });   
                }
                this.fillCategorySelect();
                this.fillParentSelect();
                if( !this.model.isNew() ){
                    this.fillChildSelect();
                }
            },

            fillCategorySelect: function(){

                var list = [], id = '', self = this;
                _.each(App.AssetModel.get('brancheCategories'), function(item){
                    list.push(item);
                })
                
                if( !this.model.isNew() && this.model.get('category') ){
                    if( this.model.get('category') && this.model.get('category').id ){
                        id = this.model.get('category').id;
                    }
                }

                var $select = this.$('select.category');
                this.appendItems(list, $select, id);
            },

            fillParentSelect: function(){

                var parentList = [], id = '', self = this;
                _.each(App.AssetModel.get('brancheParticipationTypes'), function(item){
                    if( !item.parent ){
                        parentList.push(item);
                    }
                })
                
                if( !this.model.isNew() && this.model.get('participationType') ){

                    if( this.model.get('participationType').parent && this.model.get('participationType').parent.id ){
                        id = this.model.get('participationType').parent.id;
                        
                    }else{
                        id = this.model.get('participationType').id;
                        this.$('select.sub-participationType').closest('.input-cnt').addClass('hide');
                    }
                }

                var $select = this.$('select.participationType');
                if( id == 2 ){
                    this.participationHide = true;
                }
                this.appendItems(parentList, $select, id);
            },

            fillChildSelect: function(){
                
                var childList = [], id = '', self = this;
                
                if( !this.model.isNew() && this.model.get('participationType') ){
                    id = this.model.get('participationType').id;

                    _.each(App.AssetModel.get('brancheParticipationTypes'), function(item){
                        if( item.parent && self.model.get('participationType').parent && ( self.model.get('participationType').parent.id == item.parent.id ) ){
                            childList.push(item);
                        }
                    })
                }

                var $select = this.$('select.sub-participationType');
                this.appendItems(childList, $select, id);
            },

            onChangeParticipationType: function(e){

                var id = $(e.currentTarget).val(),
                    list = [];
                
                _.each(App.AssetModel.get('brancheParticipationTypes'), function(item){
                    if( item.parent && (item.parent.id == id) ){
                        list.push(item);
                    }
                });
                var $select = this.$('select.sub-participationType');

                var wrapper = $select.closest('.input-cnt');
                if(list.length == 0){
                    wrapper.addClass('hide');
                    wrapper.removeClass('show');
                    this.participationHide = true;
                }else{
                    wrapper.removeClass('hide');
                    wrapper.addClass('show');
                    this.participationHide = false;
                }

                this.appendItems(list, $select, 0);
            },
            
            getObject: function(){

                var position = '', obj = this.$('form').serializeJSON();

                if( this.participationHide ){
                    var participationType = $('select.participationType').val();
                }else{
                    var participationType = $('select.sub-participationType').val();
                }

                obj.participationType = {
                    'id' : participationType
                }

                if( this.mainMapView && this.mainMapView.map ){
                    position = this.mainMapView.getPosition();

                    if( position.latitude && position.longitude ){
                        obj.location = position;
                    }
                }

                return obj;
            },            
        });

        return FormView;
    });
