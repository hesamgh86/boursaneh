define([
    
    'core/application',
    'core/components/base_page',   
    'core/components/base_map',   
    'models/user',
    'text!components/users/tpls/page.html',  
    'components/users/views/form',  
    'bootbox',
    'lightBox',

    ], function( Application, BasePageView , MapView , Model, PageTpl ,FormView ,bootbox) {

        var PageView = BasePageView.extend({
            name            : 'user',

            events: function(){

                var parentEvents = BasePageView.prototype.events;
                if(_.isFunction(parentEvents)){
                      parentEvents = parentEvents();
                }
                return _.extend({},parentEvents,{                    
                });
            },
            
            initialize: function(options) {
                
                if( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }
                if( this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if( this.options && this.options.id ){
                    this.model.set({ id: this.options.id });
                }
                PageView.__super__.initialize.call(this, this.options);
            },
            
            getTemplate: function () {
                return _.template(PageTpl);
            },

            setEdit: function(e){

                this.formView = new FormView({
                        model   : this.model,
                    });
                this.formView.render();
                this.formView.afterAppend();
            },
            
            setRemove: function () {
                var $this = this;                
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        bootbox.prompt({
                            title: "دلیل حذف این شهروند را در کادر زیر بنویسید :",
                            inputType: 'textarea',
                            callback: function (result) {
                                if( result ){
                                    if( result != "" ){
                                        var data = { reason : result };
                                        $.ajax({
                                            url: Application.URL + '/api/users/' + $this.model.get('id'),
                                            type : "DELETE",
                                            contentType: 'application/json',
                                            data : JSON.stringify(data),
                                            dataType: 'json',
                                            success: function(data) {
                                                Application.showNotify( {
                                                    name: 'success',
                                                    msg: 'با موفقیت حذف شد',
                                                    title: '',
                                                    delay : 100
                                                });
                                                $this.changeViewCitizens();
                                            }
                                        });
                                    }    
                                }else{
                                    if( result == "" ){
                                        Application.showNotify( {
                                            name: 'error',
                                            msg: 'لطفا دلیل حذف این شهروند را بنویسید',
                                            title: '',
                                            delay : 100
                                        });
                                        return false;
                                    }

                                }
                            },
                            buttons: {
                                confirm: {
                                    label: 'ذخیره',
                                },
                                cancel: {
                                    label: 'بستن',
                                }
                            },
                        });
                    }
                });
            },               



        });

        return PageView;
    });
