/* this is Project Form...*/
define([

    'core/application',
    'core/components/base_view',
    'models/report',
    'text!core/template/modal.html',
    'text!core/template/item.html',

    ], function( Application, BaseView, Model, MainTpl, ItemTpl ) {
        var ListModal = BaseView.extend({
            className   : 'modal-dialog modal-lg',
            name        : '',
            events      : {
                'click .item' : 'show',
            },

            initialize: function() {

               this.render();
            },

            render: function(){

                var tpl = _.template(MainTpl);
                this.$el.html(tpl(this.options));

                if( this.afterRender ){
                    this.afterRender();
                }

                return this;
            },

            showModal: function(e){

                $("#myModal").html(this.$el).modal("show");
            },

            afterRender: function(){

                this.renderItems();
                this.showModal();
            },

            renderItems: function(){

                var self = this,
                    listTpl = '';

                _.each(this.list, function(model){
                    listTpl += self.renderItem(model);
                });

                this.$('.sub-tpl').html(listTpl);
            },

            setStatusProjectOptions: function(model){

                return {

                    subType     : this.subType,
                    subTypeId   : this.subTypeId,
                    model       : model.toJSON()
                };
            },

            renderItem: function(model){

                var itemTpl = '', tpl = '', opt = {};
                var sL = '', pT = '', urgent = '', projectTaskIcon='';
                var type = this.type.toLowerCase();
                tpl = _.template(ItemTpl);

                switch( this.type ){
                    case 'Project':
                        type = "projects";
                        projectTaskIcon   = 'icon-packages';
                        var firstTask     = (model.get("tasks") && model.get("tasks").length) ? model.get("tasks")[0] : null;
                        var startLocation = '';
                        var startLocation = (firstTask && firstTask.startLocations && firstTask.startLocations.length) ? firstTask.startLocations[0] : null;


                        if( startLocation ) {
                            sL = startLocation.city.province.name + '/' + startLocation.city.name;
                            if (startLocation.isInternal) {
                                sL = sL + ' (داخل محدوده)';
                            }
                        }
                    break;

                    case 'Task':
                        projectTaskIcon   = 'icon-task_cargoType';
                        var startLocation = (model.get("startLocations") && model.get("startLocations").length > 0) ? model.get("startLocations")[0] : null;
                        if( startLocation ) {
                            sL = startLocation.name || startLocation.city.name;
                        }
                        if(model.get("is_urgent") == '1'){
                            urgent = 'فوری'
                        }else{
                            urgent = ''
                        }
                    break;
                }

                pT = model.get("id") + ' (دربستی)';
                if (model.get('type') == 1) {
                    pT = model.get("id") + ' (خرده)';
                }

                itemTpl = tpl({
                    sL                : sL,
                    pT                : pT,
                    project_task_icon : projectTaskIcon,
                    urgent            : urgent,
                    type              : type,
                    model             : model.toJSON()
                });

                return itemTpl;
            },

            getDetails: function(id){


            },

            showDetails: function(model){


            },

        });

        return ListModal;
    });
