define([

    'core/application',
    'core/model/base',

    ], function(Application, BaseModel) {

        var Model = BaseModel.extend({
            defaults: {
                name : '',
            },


            itemValidation: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
            },


            url : function(){
                return '/api/auth/session.json';
            },

            updateExecutive : function(data ,callback){
                var $this = this;
                $.ajax({
                    url: Application.URL + "/api/executives/" + data.id ,
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function(resp, val) {
                        callback && callback();
                    }
                }, this);
            },
            updateContractor : function(data ,callback){
                var $this = this;
                $.ajax({
                    url: Application.URL + "/api/contractors/" + data.id ,
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function(resp, val) {
                        callback && callback();
                    }
                }, this);
            },
            updateOperator : function(data ,callback){
                var $this = this;
                $.ajax({
                    url: Application.URL + "/api/operators/" + data.id ,
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function(resp, val) {
                        callback && callback();
                    }
                }, this);
            },            
          

            changePassword: function(data, callback){
                var $this = this;
                $.ajax({
                    type  : "POST",
                    url     : Application.URL + '/api/change-password',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function(resp){
                        callback && callback(resp)
                    },
                    error : function(error){
                        $('#changePass').removeClass('disabled');
                        var error = JSON.parse(error.responseText);
                        if(error.status == 400){
                            if(error.detail && error.detail.code == 805){
                                Application.showNotify({
                                    name: 'error',
                                    msg: 'رمز عبور قبلی اشتباه است',
                                    title: ''
                                })
                            }
                        }
                    }
                });
            }

        });
        return Model;
    });
