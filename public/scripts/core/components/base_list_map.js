define([
    
    'core/application',
    'core/components/base_view', 
    'components/maps/views/pin_item', 
    'components/maps/views/list_modal',

    ], function(Application, BaseView, ItemMap, ListModal ) {
        var ListView = BaseView.extend({
            mapType         : 1,
            lastSection     : '',
            clusterStyles   : [{
                textColor       : "#444",
                height          : 34,
                width           : 34,  
            }],

            initialize: function(options) {

                _.bindAll(this, 'renderItems', 'renderItem', 'setClusterEvent', 
                    'clusterCallback', 'showModal', 'checkModelType', 'reset' );

                if ( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }

                if ( this.options && this.options.map ){
                    this.map = this.options.map;
                }
                
                if ( this.options && this.options.lastSection ){
                    this.lastSection = this.options.lastSection;
                }

                if ( this.options && this.options.typeId ){
                    this.typeId = this.options.typeId;
                }

                if( this.collection ){
                    this.collection.unsetFilters();
                    this.collection.setFilters({ pin: 1, typeId : this.typeId });
                    
                    this.collection.on('add',           this.renderItem,    this);
                    this.collection.on('reset',         this.reset,         this);
                    this.collection.on('fetchSuccess',  this.renderItems);
                }

                this.beforeRender();
            },

            beforeRender: function(){
                
                this.collection.fetch();                
            },
            
            afterRender: function(){},

            renderItems: function(resp, collection){

                var self = this;

                this.beforeRenderItems();

                _.each(collection.models, function(model){
                    self.renderItem(model);
                });

                this.afterRenderItems();
            },

            checkModelType: function(model){
                this.renderItem(model);
            },

            renderItem: function(model){
                
                this.itemView = new ItemMap({
                    model       : (model.toJSON ? model.toJSON() : model),
                    collection  : this.collection || '',
                    map         : this.map,
                    pinListView : this,
                    typeId      : this.typeId,
                });

                this.itemView.render();

                if( this.cluster ){
                    this.cluster.pushMarkerTo_(this.itemView.marker);
                }
            },
            
            beforeRenderItems: function(){
                
                this.cleanCluster();
                this.setCluster();
                this.setClusterEvent();
            },

            reset: function(){

                this.cleanCluster(); 
            },

            afterRenderItems: function () {
                
                if( this.cluster ){
                    this.markers = this.cluster.markers_;   
                }
            },

            cleanCluster: function(){

                if( this.cluster ){
                    this.deleteMapMarker(this.markers);
                    this.markers = [];
                    this.cluster.clearMarkers();
                }
            },
            
            setCluster: function(){

                if( this.collection.length ){
                    this.cluster = new MarkerClusterer( this.map, [], this.getClusterOptions(true) );
                }    
            },

            getClusterOptions: function(){
                return {};
            },

            setClusterEvent: function(){
                
                var self = this;
                if( this.cluster ){
                        google.maps.event.addListener(this.cluster, 'clusterclick', this.clusterCallback);
                }
            },

            clusterCallback: function(cluster){

                var markerList = [], list = [];
                markerList = cluster.getMarkers();
            
                this.cluster.prevZoom_ = this.map.getZoom();
                
                for(var k in markerList){
                    list.push( this.collection.get( markerList[k].model_id ) );
                }

                if( list.length > 1 )
                    this.showModal(list);

                return false;
            },

            showModal: function(list){
               
            },

            deleteMapMarker: function(stack) {

                this.setMapOnAll(null, stack);
                stack = [];
            },

            setMapOnAll: function(map, stack) {
                for(var i in stack){
                    stack[i].setMap(map);
                }
            },
        });

        return ListView;
    });
