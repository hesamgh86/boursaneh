define([
    'core/application',
    
], function(Application) {

    var BaseRouter = Backbone.Router.extend({
        previousRoute   : '',
        currentRoute    : '',
        routeOrigin     : '',
        isPage          : false,

        before: function(route) {

            var section = route,
                sectionNotS = route,
                detailSection = route,
                format  = format = /\/:/g;

            if( format.test(route) ){
                this.isPage = true;
                detailSection = $.trim( route.split('/:')[0] ) ;
                section = $.trim( route.split('/:')[0] ) + 's';
                sectionNotS = $.trim( route.split('/:')[0] );
            }
            this.currentRoute = section;
            $('body').addClass("section-" + section );
            if(  ( $(window).width() < 768 ) ){
                $('body').addClass("page-sidebar-closed");
                $('.library-menu').addClass("active");
            }else{
                $('body').removeClass("page-sidebar-closed");
                $('.library-menu').removeClass("active");
            }
            
            this.changeActiveNavbar(sectionNotS);

            if ( this.view && this.view.close ) {
                $('body').removeClass("section-" + this.view.name);
                $('body').removeClass("section-" + this.view.currentRoute);
                
                this.view.close();
            }

            return true;
        },

        after: function(section){
            // $('[data-toggle="tooltip"]').tooltip();
        },
        
        changeActiveNavbar: function(section){

            $('.page-sidebar li.active').removeClass('active');
            // $('.navbar-me li.dropdown-notifications.open').removeClass('open');

            if( section == 'notification/suggestion' ){
                section = 'notification';
            }else if( section == 'notification/order' ){
                section = 'notification';
            }
            $('.'+ section +'-navbar').addClass('active');

        },

        showView: function(view, opt) {
            var opt = opt || {};
            $('.popover, .tooltip, .modal-backdrop, #myModal').remove();
            
            if (typeof view == 'string') {
                this.requireView(view, opt);

            } else if (typeof view == "function") {
                this.view = new view(opt);
                this.changeView();

            } else if (typeof view == 'object') {
                this.view = view;
                this.changeView();
            }
        },

        requireView: function (view, opt) {

            var self = this, opts = opt;

            require([view], function(View) {
                    
                var options = opt;
                opt.currentRoute = self.currentRoute;

                self.view = new View(options);  
                self.changeView();

            });
        },

        changeView: function(){

            if (this.view){
                var el = this.view.$el;
                if (el){
                    $('.main-content').html(el);
                }
            }

            if ( this.view && this.view.afterAppend ){
                this.view.afterAppend();
            }

            if( this.afterShowView ){
                this.afterShowView();
            }

        },

        afterShowView: function () {
            
        },

        showPage: function(params){

            var self    = this,
                view    = params.view,
                model   = params.model,
                section = params.section,
                id      = params.id || 0,
                which   = params.which || '',
                type    = 0;

            $('.popover, .tooltip, .modal-backdrop, #myModal').remove();
            
            require([view, model], function(View, Model) {
                var model = new Model({"id" : id});
                self.view = new View({
                    id: id,
                    type: type,
                    which: which ,
                    model : model

                });
                self.showPageView(model, true);
                //self.fetchItem(id);
            });
        },

        fetchItem: function(id){

            var self = this;
            
            if( id ){
                this.view.model.set({id: id}, {silent: true});    
            }
            
            this.view.model.fetch({
                // async: false,
                success: function(model,resp){
                    self.showPageView(model, true);
                },
                error: function(m,e){
                    self.showPageView('', false);  
                    JSON.parse(e.responseText);
                }
            });      
        },

        showPageView: function(model, type){

            if (this.view){

                if( type ){
                    //this.view.model = model;

                    var el = this.view.$el;
                    if (el){
                        $('.main-content').html(el);
                    } 
                }
                
                if( !type ){
                    
                    this.view.isModelFound = false;    
                    
                    switch( this.view.name ){
                        case 'project':
                            this.navigate('#dashboard', {trigger: true});
                        break;
                    }
                }
            }

            if ( this.view && this.view.afterAppend ){
                this.view.afterAppend();
            }

            if( this.afterShowView ){
                this.afterShowView();
            }

        },

        
    });
    return BaseRouter;
});

