define([

    'core/application',
    'core/components/base_section',
    'components/articles/views/list',
    'components/articles/views/date_list',
    'components/articles/views/form',
    'text!components/articles/tpls/main.html',
    
    ], function(Application, BaseSection, ListView, DateListView, FormView, MainTpl) {
        var MainView = BaseSection.extend({
            name  : 'articles',
            showDates: false,
           
            initialize: function(options) {
                
                if( this.options && this.options.date ){
                    this.showDates = true;
                }

                MainView.__super__.initialize.call(this, this.options);
            },
            
            beforeRender: function(){
                this.render();
            },

            render: function(){

                var tpl = this.getTemplate();
                this.$el.html(tpl({
                    Application : Application,
                    showDates : this.showDates
                }));
                this.afterRender();
                return this;
            },

            afterRender: function(){
                this.createListView();
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },
            
            getListView: function(){

                if( this.showDates ){

                    return new DateListView({
                        dateStr: this.options.date
                    });

                }

                return new ListView({
                    status : this.status,
                    mode : 'articles',
                });
            },

            afterAppend: function(){},

            showForm: function(){
                this.form = new FormView();
                this.form.render();
                this.form.afterAppend();
            }
        });

    return MainView;
});