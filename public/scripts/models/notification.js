define([

    'core/application',
    'core/model/base',

    ], function(Application, BaseModel) {

        var Model = BaseModel.extend({
            name: 'notification',
            defaults: {
            },
                
            url : function(){
                return Application.URL + '/api/notifications/' + this.id;
            },
            parse : function(obj){
                var obj = Model.__super__.parse.call(this, obj);

                var url = (obj.refType == 1) ? "#task/" : "#projects/";
                url += obj.refId;
                //Offer for freightage owner
                if(obj.action == 7){
                    url += "/status";//task, freight owner , rannade entekhab shod
                }
                if(obj.action == 1){
                    url += "/status"; //task, freight owner , rannade entekhab shod
                }
                if(obj.action == 5){
                    url += "/suggestions";
                }
                if(obj.action == 9){
                    url += "/status";
                }
                obj.url = url;
                return obj;
            }

        });
        return Model;
    });

