define([
    'core/application',

    /* plugins */
    'bootstrapselect',
    'persiandate',
    'persiandatepicker',
    'bootbox',
    'ripple',

], function( Application, BootstrapSelect, PersianDate, PersianDatePicker, bootbox ) {

    var BaseView = function(options) {

        this.bindings = [];
        Backbone.View.apply(this, [options]);
    };

    _.extend(BaseView.prototype, Backbone.View.prototype, {
        className       : 'content-me',
        typeAppend      : 'append',
        setInputsValues : true,
        
        afterAppend: function(){
            this.initDatepickers();
            this.initTimepickers();
            this.initSelectPicker();
            if(!this.setInputsValues){
                $(".date-picker, .time-picker", this.$el).val("")
            }
        },
        clearForm: function(e){
            var form = $("form:visible", this.$el);
            $('select:not([name=myDatatable_length])', form).selectpicker('deselectAll');
            $('input', form).val('');
        },
        
        initDatepickers:function(elements){
            var elements = elements ||  $(".date-picker", this.$el);
            elements.each(function(index, el){
                $(el).persianDatepicker({
                    altFormat   : "YYYY MM DD HH:mm:ss",
                    observer    : true,
                    format      : 'YYYY/MM/DD'
                });
            });
        },

        initTimepickers:function(elements){
            var elements = elements ||  $(".time-picker", this.$el);
            elements.each(function(index, el){
                $(el).persianDatepicker({
                    altFormat       : "YYYY MM DD HH:mm:ss",
                    observer        : true,
                    format          : "HH:mm",
                    onlyTimePicker  : true
                });
            });
        },
        initSelectPicker: function(){

            var self = this;
            _.each(this.$('select:not([name=myDatatable_length])'), function(el){

                $(el).selectpicker('refresh');
            });
        },

        checkIsNumber: function(value){

            if( value && value.match(/^\d+$/) ){
                return true;

            }else{
                return false;
            }
        },

        convertToString: function(list){

            var str = '',
                list = _.pluck(list, 'id');

            if( list && list.length > 0 ){
                str = list.join(',');
            }

            return str;
        },

        appendItems: function(list, $select, id){

            var id = id || 0 , self = this;
            $select.empty();

            _.each(list, function(item){

                var tpl = self.appendItem(item, id);                
                $select.append( $(tpl) );
            });

            $select.selectpicker('refresh');
        },

        appendItem: function(item, id){

            var name = '', modelId = item.id, tpl = '';

            if( item.name ){
                name = item.name;

            }else if( item.title ){
                name = item.title;

            }else if( item.firstname && item.lastname ){
                name = item.firstname + ' ' + item.lastname;

            }else if( item.username ){
                name = item.username;
            }

            if( item && !item.id && item.cid ){
                modelId = item.cid;
            }
            
            if( modelId == id ){
                tpl = '<option value="'+ modelId +'" selected>' + name + '</option>';

            }else{
                tpl = '<option value="'+ modelId +'">' + name + '</option>';
            }

            return tpl;
        },
       
        changeToPersianMoney: function(type, value, mode, el){

            if( value ){

                if( typeof value == "number"){
                    value = value.toString();
                }
                if( value ){

                    if( !el ){
                        el = $('[name='+ type +']');
                        if( el ){
                            el = el.parent();
                        }
                    }

                    $('[name='+ type +']', el).val(value);
                    var res = this.addComma(value);
                    $('[data-type='+ type +']', el).val(res);
                }
            }

        },

        convertMiladiToPersion :function(val){
            if(val){
                var value = new Date( val ) ,
                    change =  persianDate(value).pDate;

                return persianDate([change.year, change.month, change.date , change.hours , change.minutes]).format("YYYY/MM/DD HH:mm");
            }
        },

        /*
         * http://stackoverflow.com/questions/2254185/regular-expression-for-formatting-numbers-in-javascript
         */
        addComma: function(str){

            return str.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        },

        /*
         * http://stackoverflow.com/questions/19245897/regex-to-remove-multiple-comma-and-spaces-from-string-in-javascript
         */
        removeComma: function(str){
            return str.replace(/[,]+/g, "").trim();
        },

        bindTo: function(model, ev, callback) {

            model && model.bind(ev, callback, this);
            this.bindings.push({
                model: model,
                ev: ev,
                callback: callback
            });
        },
        unbindFromAll: function() {
            _.each(this.bindings, function(binding) {
                binding.model.unbind(binding.ev, binding.callback);
            });
            this.bindings = [];
        },

        close: function() {

            $('#myModal').modal('hide');
            $('.modal-backdrop').remove();
            this.onResizeWindow && $(window).off("resize", this.onResizeWindow);
            this.stopListening();
            this.unbindFromAll();
            this.unbind();
            this.remove();
        },

        closeModal: function() {

            $('#myModal').modal('hide');
            this.unbind();
            this.remove();
        },

        disposeSubViews: function(items, allItems) {

            items = allItems ? this.subViews : items;

            for (var index in items) {
                this.subViews[index].close();
                delete this.subViews[index];
            }
        },

        loadMore: function(){},

        bindingScripts: function(){
            if( $('[data-toggle="tooltip"]') && ( $('[data-toggle="tooltip"]').length > 0 ) ){
                $('[data-toggle="tooltip"]').tooltip({
                    container : 'body',
                });
            }


            $.ripple(".btn", {
                debug: false, // Turn Ripple.js logging on/off
                on: 'mousedown', // The event to trigger a ripple effect

                opacity: 0.4, // The opacity of the ripple
                color: "auto", // Set the background color. If set to "auto", it will use the text color
                multi: false, // Allow multiple ripples per element

                duration: 0.7, // The duration of the ripple

                // Filter function for modifying the speed of the ripple
                rate: function(pxPerSecond) {
                    return pxPerSecond;
                },

                easing: 'linear' // The CSS3 easing function of the ripple
            });             

        },

        getDatepickerValue :function(input){

            var date_str = input.val();
            if(date_str){
                date_str = date_str.toEnDigit();
                var array = date_str.split('/');
                return persianDate([parseInt(array[0]), parseInt(array[1]), parseInt(array[2])]).gDate;
            }
            return;
        },

        isNumberKey: function(evt){
            
            var charCode = (evt.which) ? evt.which : event.keyCode

            if(charCode >= 1776 && charCode <= 1785){
                return true
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }

            return true;
        },

        converPrsianNumber: function(e){
            var input = $(e.target);
            var value = input.val();
            if(value != ""){
                input.val(value.toEnglishDigits())
            }
        },

        changeMoney: function(e){
            var input = $(e.target);
            var inputValue = input.val();
            var target = input.data("target");
            var mainInput = $(target, this.$el)

            if(inputValue != ""){
                //inputValue = inputValue.toEnglishDigits();
                var value = inputValue.replace(/,/g, ''),
                    num = value.replace(/(\s)/g, '');

                input.val(num.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
                mainInput.val(value);
            }else{
                mainInput.val("");
            }
        },
    });

    Backbone.View = (function(View) {
        return View.extend({
            constructor: function(options) {
                _.extend(this, options);
                this.options = options || {};
                View.apply(this, arguments);
            }
        });
    })(Backbone.View);

    _.mixin({
        getLocationTitle:function(obj){
            if(obj && obj instanceof Object){
                if(obj.isInternal){
                    return (obj.province && obj.province.name) || "" + "/" + (obj.city && obj.city.name) || "" ;
                }else{
                    return obj.address ? (obj.address) : "";
                }
            }else{
                return "";
            }

        },
        getCommaSeperetadePrice: function(value){
            value = String(value || 0)
            if(value != "" && value != "0"){
                var _value = value.replace(/,/g, ''),
                    num = value.replace(/(\s)/g, '');

                 return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            }else{
               return ""
            }
        },
    })

    BaseView.extend = Backbone.View.extend;
    return BaseView;
});
