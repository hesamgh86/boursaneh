define([

    'core/application',
    'core/components/base_form',  
    'models/news',
    'text!components/news/tpls/form.html',


    'persiandate',
    'persiandatepicker',
    'bootstrapdatetimepicker',

    ], function( Application, BaseFormView, Model, FormTemplate, PersianDate, PersianDatePicker , bootstrapdatetimepicker  ){
        
        var FormView = BaseFormView.extend({
            className       : 'modal-dialog modal-lg',
            name            : 'location',
            mode            : 'create',
            isNeedUpload    : true,

            events: function(){

                var parentEvents = BaseFormView.prototype.events;
                if(_.isFunction(parentEvents)){
                      parentEvents = parentEvents();
                }
                return _.extend({},parentEvents,{
                    'click .add-answer'             : "addAnswer",
                    'click .remove-answer'          : 'removeAnswer',
                    'click .btn-add-upload'         : 'uploadTrigger',
                    'click .btn-img-remove'         : 'removeImage',
                    'click .toggle[name=status]'    : 'toggleCheck',                                        
                });
            },
            
            initialize: function(options) {
                
                _.bindAll(this, 'removeAnswer');
                if(this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if(this.options && this.options.modalHolder ){
                    this.modalHolder = this.options.modalHolder;
                }

                
                FormView.__super__.initialize.call(this, this.options);
            },

            afterAppend: function(){
                BaseFormView.__super__.afterAppend.call(this, this.options);

                if(this.model.isNew()){
                    // this.$('.start-date').val(''); 
                    this.$('.end-date').val(''); 
                }else{
                    if(!this.model.get('startDate')){
                      // this.$('.start-date').val('');  
                    }
                    if(!this.model.get('endDate')){
                      this.$('.end-date').val('');  
                    }
                }

                this.sendUpload();
                $('.datetimepicker').datetimepicker({
                    format: 'HH:mm',
                }); 

                if( !this.model.isNew() ){
                  this.toggleCheckEdit();  
                }             
            },

            getTemplate: function(){
                return _.template(FormTemplate);
            },

            validateModel: function(model){
                
                _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

                if (model.itemValidation) {
                    var _itemValidation = typeof model.itemValidation == 'function' ? model.itemValidation() :  model.itemValidation
                    model.validation = _.extend((model.validation || {}), _itemValidation);
                }

                _.extend(model, Backbone.Validation.mixin);

                if (errors = model.validate()) {
                    if( Object.keys(errors)[0] == "minAge" || Object.keys(errors)[0] == "maxAge"){
                        Application.showNotify({
                            name: 'error',
                            msg: 'حداقل سن باید کوچکتر از حداکثر سن باشد',
                            title: 'خطا'
                        });
                    }else if( Object.keys(errors)[0] == "startDate" ){
                        if( Object.values(errors)[0] == "Start date is required" ){
                            Application.showNotify({
                                name: 'error',
                                msg: 'تاریخ شروع و زمان شروع باید مشخص باشد',
                                title: 'خطا'
                            });                                
                        }else{
                            Application.showNotify({
                                name: 'error',
                                msg: 'تاریخ اتمام باید بعد از تاریخ شروع باشد',
                                title: 'خطا'
                            });
                        }
                    }else{
                        Application.showNotify({
                            name: 'error',
                            msg: 'لطفا اطلاعات هر بخش را کامل وارد کنید',
                            title: 'خطا'
                        });
                    }

                    $('[type=button].disabled', this.$el).removeClass('disabled')
                    this.inValid(errors, model);
                    return false;
                }

                return true;
            },            

            addAnswer: function(e){
                
                var tpl =  '<div class="input-line clearfix answer-holder" style="position: relative;">\
                                <div class="col-md-12">\
                                    <div class="input-cnt form-control clearfix">\
                                        <span class="lable-input">گزینه</span>\
                                        <input class="input-input" value="" >\
                                    </div>\
                                </div>\
                                    <div class="toggle-answer remove-answer color-red transition">\
                                        <i class="fa fa-close" aria-hidden="true"></i>\
                                    </div>\
                            </div>';

                this.$('.answer-holder:last').after(tpl);
                this.$('.answer-holder:last input').focus();
                $( '.remove-answer' , this.$('.answer-holder:last') ).on('click', this.removeAnswer, this);
            },

            removeAnswer: function(e){
                
                this.$(e.currentTarget).closest('.answer-holder').remove();
            },

            getObject: function(){
                var obj = this.$('form#newsForm', this.$el).serializeJSON(),
                    $this = this;

                var start_date = $("input.start-date", this.$el).val();
                if(start_date){
                    start_date = this.getDatepickerValue( $("input.start-date", this.$el));
                    start_date = moment(start_date).format('YYYY/MM/DD');
                    start_date = start_date + ' ' + $("input.start-time", this.$el).val();
                    start_date = new Date(start_date);
                    var date_utc = new Date(start_date.getUTCFullYear(), start_date.getUTCMonth(), start_date.getUTCDate(),  start_date.getUTCHours(), start_date.getUTCMinutes(), start_date.getUTCSeconds());
                    obj.startDate = moment(date_utc).format("YYYY-MM-DD HH:mm:ss");
                }

                var end_date = $("input.end-date", this.$el).val();
                if(end_date){
                    end_date = this.getDatepickerValue( $("input.end-date", this.$el));
                    end_date = moment(end_date).format('YYYY/MM/DD');
                    end_date = end_date + ' ' + $("input.end-time", this.$el).val();
                    end_date = new Date(end_date);
                    var date_utc = new Date(end_date.getUTCFullYear(), end_date.getUTCMonth(), end_date.getUTCDate(),  end_date.getUTCHours(), end_date.getUTCMinutes(), end_date.getUTCSeconds());
                    obj.endDate = moment(date_utc).format("YYYY-MM-DD HH:mm:ss");
                }             


                var list = this.$('.answer-holder');
                var answers = [];
                _.each(list, function(item){
                    var val = $(item).find('input').val();
                    if( val ){
                        answers.push( val );
                    }
                    
                });

                if( obj ){
                    obj.answers = answers;
                }

                if( obj && obj.status == 'none' ){
                    obj.status = '';
                }

                if( obj ){
                    if( obj.status ){
                        obj.status = '0';
                    }else{
                        obj.status = '2';
                    }

                    if( obj.isSecure ){
                        obj.isSecure = '1';
                    }else{
                        obj.isSecure = '0';
                    }
                }
                return obj;
            }, 
             
            uploadTrigger: function(e){
                this.$('#fileupload').click();
            }, 

            sendUpload: function(){

               var self        = this,
                    fileInput   = '',
                    zone        = '';

               if( this.$('#dropzone') ){
                    zone = this.$('#dropzone');
                }
                
               if( this.$('#fileupload') ){
                    fileInput = this.$('#fileupload');
                }

               fileInput.fileupload({
                    dropZone : $('body'),
                    dragover: function(e){
                        
                       var dropZone = $('body'),
                            timeout = window.dropZoneTimeout;
                        if (timeout) {
                            clearTimeout(timeout);
                        } else {
                            dropZone.addClass('in');
                        }
                        var hoveredDropZone = $(e.target).closest(dropZone);
                        dropZone.toggleClass('hover', hoveredDropZone.length);
                        window.dropZoneTimeout = setTimeout(function () {
                            window.dropZoneTimeout = null;
                            dropZone.removeClass('in hover');
                        }, 100);
                    },

                   add      : function(e,data){
                        
                       data.submit();
                    },

                   always  : function(e, data) {},

                   progress: function(a,b,c){
                        self.$('.progress').removeClass('hide');
                    },

                   progressall: function (e, data) {

                       var progress = parseInt(data.loaded / data.total * 100, 10);
                        self.$('.progress .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                    },
                    
                   done    : function(e,data){
                        
                       var item = '', res = '', file = '', path = '';

                       if( e ){
                            e.typeAppend = 'prepend';
                        }
                        
                       if( data &&  data.result && data.result[0] ){
                            
                           res = data.result[0];
                            file = data.files[0];                        
                       }

                       if( data && data.result && data.result.image ){
                            path = data.result.image;
                        }

                        self.$('#docInput').val(path);
                        self.model.set({documents: path}, {silent: true});
                        self.$('.figure-license img').attr({src: Application.URLIMAGE + path});
                        self.$('.figure-license.hide').removeClass('hide');
                        self.$('.progress').addClass('hide');
                        $('.fa-image').removeClass('show');
                        $('.fa-image').addClass('hide');
                    },

                   fail: function(e,data){
                        Application.showNotify(0);
                    }
                });
            },
            
            removeImage: function(e){
                $('.figure-license img').attr('src','');
                $('[name=image]').val('');
                $('.fa-image').removeClass('hide');
                $('.fa-image').addClass('show');
            },  

            toggleCheck: function(e){
                el = $(e.target);
                if( el.prop('checked') ){
                    $('.time-date input.date-picker').prop("disabled", true);
                    $('.time-date input.datetimepicker').prop("disabled", true);
                    $('.time-date input.date-picker').val('');
                    $('.time-date input.datetimepicker').val('');                   
                }else{
                    $('.time-date input.date-picker').prop("disabled", false);
                    $('.time-date input.datetimepicker').prop("disabled", false);
                    var newDate  = persianDate().format('YYYY/MM/DD');
                    $('.time-date input.start-date').val(newDate);
                }
            },
            toggleCheckEdit: function(){
                if( this.model.get('status') == 0 ){
                    $('.time-date input.date-picker').prop("disabled", true);
                    $('.time-date input.datetimepicker').prop("disabled", true);
                    $('.time-date input.date-picker').val('');
                    $('.time-date input.datetimepicker').val('');
                }else if( this.model.get('status') == 2 ){
                    $('.time-date input.date-picker').prop("disabled", false);
                    $('.time-date input.datetimepicker').prop("disabled", false);
                    var newDate  = persianDate().format('YYYY/MM/DD');
                    $('.time-date input.start-date').val(newDate);
                }
            },
        });

        return FormView;
    });
