define([

    'core/application',
    'core/components/base_view',
    'text!core/template/loading.html',

    /* plugins */
    'datatables.net',
    'datatablebootstrap',
    'datatablebuttons',
    'datatableselect',

    ], function( Application, BaseView, LoadingTemplate ) {
        var BaseTableListView = BaseView.extend({
            typeAppend          : 'append',
            mode                : 'create',
            listHolder          : '#myDatatable',
            items               : [],
            mainAjaxApi         : '',
            ajaxApi             : '',
            isSelectAll         : false,
            serverSideDatatable : false,

            events:{

                'keyup textarea'            : 'countChars',
                'click .toggle-select li a' : 'toggleSelect',
                'click .btn-send'           : 'sendItems',
            },

            initialize: function() {

                _.bindAll(this, 'toggleChecked', 'saveSuccessCallback', 'errorCallback', 'fnDrawCallback' );
                
                if( this.options && this.options.listHolder ){
                    this.listHolder = this.options.listHolder;
                }

                this.beforeRender();
                this.render();          
            },

            beforeRender: function(){},

            render: function(){

                var tpl = this.getTemplate();
                this.$el.html(tpl());
                this.afterRender();
                return this;
            },

            afterRender: function(){},
            afterAppend: function(){
                
                this.getTable();
            },

            refresh: function(obj1, obj2){

                this.ajaxApi = this.mainAjaxApi;
                
                var keys = Object.keys(filters), data = [];

                for( var i in keys ){
                    var f = keys[i] + '=' + filters[keys[i]];
                    data.push(f);
                }

                if( data.length > 0 ){
                    var opt = data.join('&');
                    this.ajaxApi += '&' + opt;
                }
                
                if( this.table ){
                    this.table.destroy();
                }
                
                this.getTable();
            },

            addParamsAjaxRequest: function(){

                var list = _.pluck(this.itemModel.get('endLocations'), 'province');
                var ids = _.pluck(list, 'id');
                if( ids ){
                    this.ajaxApi += '&filter[driveToProvinces]=' + ids.join(',');
                }
                
                ids = '';
                ids = _.pluck(this.itemModel.get('vehicleTypes'), 'id');
                if( ids ){
                    this.ajaxApi += '&filter[vehicleTypes]=' + ids.join(',');
                }
            },

            getTable: function(){

                var self = this, opt = '', columnDefs = [], aoColumns = [], 
                    loadingTpl = _.template(LoadingTemplate);

                if ( $.fn.dataTable.isDataTable( this.listHolder ) ) {
                    this.table.ajax.reload( null, false );
                }
                
                opt = this.getTableOptions();
                
                if( !opt.aoColumns ){
                    aoColumns = [{ "mDataProp":  'name' }];
                }else{
                    aoColumns = opt.aoColumns;
                }

                if ( !opt.aoColumns ) {
                    columnDefs = [{ orderable: false, targets: -1 }];
                }else{
                    columnDefs = opt.columnDefs;
                }

                if( !columnDefs.render ){
                    columnDefs.render = this.toggleChecked;
                }

                this.table = $("table", this.$el).DataTable({

                    "bAutoWidth"        : false,
                    "aoColumns"         : aoColumns,
                    "columnDefs"        : columnDefs,
                    "aaSorting"         : [], 
                    "bDestroy"          : true,
                    "bFilter"           : false,
                    "bPaginate"         : true,
                    "bProcessing"       : true,
                    "bServerSide"       : true,
                    "bStateSave"        : false,
                    "iDisplayLength"    : 10,
                    "searching"         : true,
                    "sPaginationType"   : "bootstrap",
                    "sDom"              : 'l<"enabled">frti<"bottom"p><"clear">',
                    "pagingType"        : "full_numbers",

                    // "sAjaxSource"       : Application.URL + this.ajaxApi,
                    "ajax": {
                        "url": Application.URL + this.ajaxApi,
                        "type": "GET",
                    },
                    "language"          : {
                        "emptyTable"    : "<span class = 'no-item'>موردی یافت نشد</span>",
                        "sProcessing"   : "<div class='loading-wrapper'><div class='loading-container'><div class='loading-circle'></div><div id='loading-text'>شکیبا باشید</div></div></div>",
                    },
                    // "fnDrawCallback"    : this.fnDrawCallback,
                }); 
            },

            fnDrawCallback: function(setting){

                var el = $(setting.nTable).find('th input.select-page') || '';
                if( el && !this.isSelectAll ){
                    $(el).prop('checked', false);

                }else if( el && this.isSelectAll ){
                    $(el).prop('checked', true);
                }
            },

            toggleChecked: function (data, type, full, meta) {
                
                if( this.isSelectAll ){

                    var row = data.split('>')[0] += ' checked=true >' 
                    data.split('>')[0]= row;
                    return row;
                }
                
                return data;
            },

            getTableOptions: function(){
                
                var aoColumns = '', columnDefs = '';

                columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns   = this.getColumns();

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },

            getColumns: function(){
                return [];
            },

            /* suppose template*/
            getTemplate: function () {
                return '';
            },
            
            getCurrentModel: function(){
                return {};
            },

            disposeAllItems : function(){
                for(var index in this.items){
                    this.items[index].remove();
                };
            },

            /* keyup on textarea */
            countChars: function(){
                
                var value = $.trim(this.$('textarea').val()) || '',
                    length  = this.$('textarea').val().length;
                    
                this.$('.textarea-holder .count').text(length);
            },

            /* toggle select */
            toggleSelect: function(e){

                var el   = this.$(e.currentTarget),
                    type = $(el).data('type') || '';

                this.$('.toggle-select .active').removeClass('active');
                this.$(el).parent().addClass('active');
                
                switch(type){
                    case 1:
                        this.selectAll();
                    break;
                    case 2:
                        this.selectPage();
                    break;
                    case 3:
                        this.unSelect();
                    break;
                }
            },

            selectAll: function(){

                this.isSelectAll = true;
                this.$('table tr input[type="checkbox"]:not(:checked)').prop({'checked': true });
            },

            selectPage: function(e){

                this.isSelectAll = false;
                this.$('table tbody input[type="checkbox"]:not(:checked)').prop({'checked': true });
            },
            
            unSelect: function(e){

                this.isSelectAll = false;
                this.$('table tbody input[type="checkbox"]').prop({'checked': false });
            },

            /* send items */
            sendItems: function(e){
                
                var idList  = [], by = '';

                if( e ){
                    by = this.$(e.currentTarget).data('type');
                    this.$(e.currentTarget).addClass('disabled');
                }

                this.$('.wrapper-bar-org.error-me').removeClass('error-me');
                
                if( !this.isSelectAll ){
                    idList = this.findSelectedItems(e);
                }

                if( !this.isSelectAll ){

                    if( idList.length == 0 ){
                        this.$(e.currentTarget).removeClass('disabled');
                        this.showNotify(6);
                        return;
                    }
                }

                var msg = $.trim( this.$('textarea').val() ) || '',
                    obj = {
                        by          : by,
                        type        : '2',
                        message     : msg
                    };

                if( !msg ){
                    this.$(e.currentTarget).removeClass('disabled');
                    this.$('.wrapper-bar-org').addClass('error-me');
                    this.showNotify(0, 'هیچ متنی برای ارسال وجود ندارد');
                    return;
                }

                if( this.isSelectAll ){
                    obj.filter =  this.collection.filters.toJSON() || {};
                    obj.byFilter = '1';
                    obj.sSearch = this.table.search() || '';

                }else{
                    obj.drivers = idList;
                    obj.byFilter = '0';
                }

                var model = this.getCurrentModel();

                model.save(obj, {
                    silent      : true,
                    success     : this.saveSuccessCallback,
                    error       : this.errorCallback,
                });                   
            },

            findSelectedItems: function(){

                var list    = this.$('table tr td'), idList  = [];

                _.each(list, function(tr){
                    
                    var id = '', isCheck = false, 
                        input = $(tr).find('input');
                    
                    if( input ){
                        
                        isCheck = $(tr).find('input').is(':checked');
                        id = $(tr).find('input').val();

                        if( isCheck ){
                            idList.push(id);
                        }
                    }
                });
                
                return idList;
            },

            saveSuccessCallback: function (a,b) {
                
                this.$('.btn-success.btn-send.disabled').removeClass('disabled');
                this.showNotify(7);
            },

            errorCallback: function (e,msg) {
                
                this.$('.btn-success.btn-send.disabled').removeClass('disabled');

                if( msg && msg.responseJSON && msg.responseJSON.detail && msg.responseJSON.detail.code && ( msg.responseJSON.detail.code == 1601 ) ){
                    this.showNotify(0, 'اعتبار سامانه پیام کوتاه شما کافی نیست');
                
                }else{
                    this.showNotify(0);
                }
            },


        });
    
        _.mixin({
           
        });

        return BaseTableListView;
});