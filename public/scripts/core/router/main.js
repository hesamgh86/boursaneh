define([

    'core/application',
    'core/router/base',
    'components/notifications/views/modal/list'
    
], function(Application, BaseRoute, NotificationMainView ) {

    var Router = BaseRoute.extend({
        routes: {

            ''                  : 'firstPage',
            'dashboard'         : 'showDashboardPage',

            'news'              : 'showNewsPage',
            'news/:id'          : 'showOneNewsPage', 

            'branches'          : 'showBranchesPage',
            'branches/:id'      : 'showOneBranchesPage',

            'users'             : 'showUsersPage',
            'users/:id'         : 'showOneUsersPage',

            'articles'          : 'showArticlesPage',
            'articles/:date'    : 'showOneArticlesPage',           
             
            'config'            : 'showConfigPage',   
            'profile'           : 'showProfilePage',
            'notifications'     : 'showNotification',
        },
        
        initialize: function() {
            // this.notificationList();            
        },
        
        firstPage: function(){            
            this.navigate("dashboard",{trigger:true});
        },

        showDashboardPage: function(){
            this.showView( 'components/dashboard/views/main' );
        },

        showArticlesPage: function(){
            this.showView( 'components/articles/views/main' );
        },
        showOneArticlesPage: function(date){
            this.showView( 'components/articles/views/main', {date: date});
        },

        showNewsPage: function(){
            this.showView( 'components/news/views/main' );
        },
        showOneNewsPage: function(id){
            this.showPage({
                view    : 'components/news/views/page',
                model   : 'models/news',
                section : 'news',
                id      : id,
                which   : 'news', 
            }); 
        }, 

        showBranchesPage: function(){
            this.showView( 'components/branches/views/main' );
        },
        showOneBranchesPage: function(id){
            this.showPage({
                view    : 'components/branches/views/page',
                model   : 'models/branche',
                section : 'branche',
                id      : id,
            }); 
        },      

        showUsersPage: function(){
            this.showView( 'components/users/views/main' );
        },
        showOneUsersPage: function(id){
            this.showPage({
                view    : 'components/users/views/page',
                model   : 'models/user',
                section : 'user',
                id      : id,
            }); 
        },
        
        showProfilePage: function(){
            
            this.showPage({
                view    : 'components/profile/views/page',
                model   : 'models/session',
                section : 'profile',
            }); 
        },

        showConfigPage: function(){
            
            this.showPage({
                view    : 'components/config/views/main',
                model   : 'models/session',
                section : 'config',
            }); 
        },

        showNotification: function(){
            this.showView( 'components/notifications/views/page/main' );
        },
        notificationList: function(){

            if( !this.notificationMainView ){
                this.notificationMainView = new NotificationMainView();
                Application.notificationsCollection = this.notificationMainView.collection;
            }

            $('.dropdown-notifications a').after( this.notificationMainView.el );
        },
        

    });

    return Router;
});
