$(document).ready(function () {
    var table = $('#example').DataTable({
        dom: 'lBfrtip',
        select: true,
        "buttons": [
        {
            extend: 'collection',
            text: 'Export',
            buttons: [
            {
                extend: 'copy',
                text: 'copy all'
            },
            {
                extend: 'csv',
                text: 'csv all'
            },
            {
                extend: 'excel',
                text: 'excel all'
            },
             {
                extend: 'excel',
                text: 'excel selected',
                exportOptions: {
                    modifier: {
                        selected: true
                    }
                }
            },
            {
                extend: 'pdf',
                text: 'pdf all'
            },
            {
                extend: 'print',
                text: 'print all'
            },
            {
                extend: 'print',
                text: 'print selected',
                exportOptions: {
                    modifier: {
                        selected: true
                    }
                }
            }
           ]
         }
       ],
       "lengthMenu": [5, 10, 25, 50, 75, 100 ]
       
    });


    $('#example tbody ').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

//delete

    $('#button').click(function (e) {
        if ($('tr.selected').length > 0) {
            var el = $('.item-row.selected');
            var id = el.data('id');

            $.ajax({
                type: "DELETE",
                url: "/delete/" + id,
                success: function () {
                    alert(" success : " + " حذف شده است. ");
                    el.remove();

                }
            });
        } else {
            alert(" error : " + "انتخاب نشده است. ");
        }
    });


//create

    $("body #addRow").on("click", function () {
        $("#myModal-create").modal();
    });


//edit

    $("body #editRow").on("click", function () {
        if ($('tr.selected').length > 0) {
            var el = $('.item-row.selected');
            var id = el.data('id');

            $.ajax({
                type: "GET",
                url: "/view/" + id,
                success: function (data) {
                    $('#myModal-update input[name=id]').val(data.id);
                    $('#myModal-update input[name=name]').val(data.name);
                    $('#myModal-update input[name=office]').val(data.office);
                    $('#myModal-update input[name=age]').val(data.age);
                    $('#myModal-update input[name=startDate]').val(data.startDate);
                    $('#myModal-update input[name=salary]').val(data.salary);
                }
            });
            
            $("#myModal-update").modal();
        } else {
            alert(" error : " + "انتخاب نشده است. ");
        }
    });

}); 