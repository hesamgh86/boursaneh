require.config({


    // urlArgs: "version=1.0",
    paths: {
        'jquery'                 : 'vendor/jquery/jquery',
        
        'underscore'             : 'vendor/underscore/underscore-min',
        'underscore.string'      : 'vendor/underscore.string/underscore.string.min',
        'backbone'               : 'vendor/backbone/backbone-min',
        'bootstrap'              : 'vendor/bootstrap/dist/js/bootstrap.min',

        'pnotify'                : 'vendor/pnotify/pnotify.custom.min',
        'text'                   : 'vendor/requirejs-text/text',
        'backbone.routefilter'   : 'vendor/backbone.routefilter-master/src/backbone.routefilter',
        'persiandate'            : 'vendor/persian-datepicker/lib/persian-date',
        'persiandatepicker'      : 'vendor/persian-datepicker/dist/js/persian-datepicker-0.4.5.min',
        'bootstrapdatetimepicker': 'vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',
        'bootstrapselect'        : 'vendor/bootstrap-select-1.12.4/dist/js/bootstrap-select.min',

        'datatables.net'         : 'vendor/DataTables/js/jquery.dataTables.min',
        'datatablebootstrap'     : 'vendor/DataTables/js/dataTables.bootstrap.min',
        'datatablebuttons'       : 'vendor/DataTablesEditor/js/dataTables.buttons.min',
        'datatableselect'        : 'vendor/DataTablesEditor/js/dataTables.select.min',

        'bootbox'                : 'vendor/bootbox/bootbox.min',
        'moment'                 : 'vendor/moment-develop/moment',

        'file-iframe'            : 'vendor/jQuery-File-Upload/js/jquery.iframe-transport',
        'jquery.ui.widget'       : 'vendor/jQuery-File-Upload/js/jquery.ui.widget',
        'fileupload'             : 'vendor/jQuery-File-Upload/js/jquery.fileupload',

        'validation'             : 'vendor/backbone.validation-master/dist/backbone-validation-amd-min',        
        'lightBox'               : 'vendor/lightbox/dist/js/lightbox',

        'highcharts'            : 'vendor/Highcharts-6.0.2/code/highcharts',
        'highchartsExport'      : 'vendor/Highcharts-6.0.2/code/modules/exporting',
        'highcharts3d'          : 'vendor/Highcharts-6.0.2/code/highcharts-3d',
        'highchartsMore'        : 'vendor/Highcharts-6.0.2/code/highcharts-more',


        'ripple'                : 'vendor/Ripple.js-develop/ripple.min',        

        'pace'                   : 'vendor/pace.min',
        'me'                     : 'vendor/me',
        
        'almond'                 : 'vendor/almond-master/almond',
        
    },
    locale: 'en-us',
    shim: {
        'underscore.string': {
            deps: ['underscore']
        },
        'bootstrap': {
            deps: ['jquery'],
        },

        'backbone': {
            deps: ['jquery']
        },
        'backbone.routefilter': {
            deps: ['backbone', 'underscore']
        },
        'persiandate':{
            deps: ['jquery']
        },
        'persiandatepicker':{
            deps: ['jquery' , 'persiandate']
        },
        'bootstrapdatetimepicker':{
            deps: ['jquery' ,'moment' ,'bootstrap' ]
        },
        'bootstrapselect':{
            deps: ['bootstrap']
        },      
        'datatables.net':{
            deps: ['jquery' , 'bootstrap']
        },
        'datatablebootstrap':{
            deps: ['jquery' , 'datatables.net']
        },
        'datatablebuttons' :{
            deps: ['jquery' , 'datatables.net']
        },
        'datatableselect' :{
            deps: ['jquery' , 'datatables.net']
        },
        'pnotify': {
            deps: ['jquery']
        },
        'bootbox':{
            deps: ['jquery' , 'bootstrap']
        },
        'fileupload':{
            deps: ['jquery','file-iframe']
        },
        'validation':{
            deps: ['underscore', 'backbone']
        },
        'lightBox':{
            deps: ['jquery' , 'bootstrap']
        },
        'highcharts':{
            deps: ['jquery' ]
        },
        'highchartsExport':{
            deps: ['jquery' , 'highcharts']
        },
        'highcharts3d':{
            deps: ['jquery' , 'highcharts']
        },
        'highchartsMore' : {
            deps: ['jquery' , 'highcharts']
        },        
        'ripple':{
            deps: ['jquery']
        },
        'pace':{
            deps: ['jquery' , 'bootstrap']
        },
        'me':{
            deps: ['jquery' , 'bootstrap']
        },

    }
});
require([
    'core/router/main',
    'core/application',
    'models/session',
    'bootstrap',
    'pace',
    'me',
    'backbone.routefilter',
    'core/boot',

], function( MainRoutes, Application, SessionModel ) {
    Application.Session = new SessionModel(session);
    Application.ajaxsetup();
    Application.Router = new MainRoutes();
    Backbone.history.start();

    window.App = Application
    Application.getPrefrences();
    Application.getLangs();

});
