define([
    'core/application',
    'backbone',
], function(Application, Backbone) {

    var BaseView = function(options) {

        this.bindings = [];
        Backbone.View.apply(this, [options]);
    };

    
    _.extend(BaseView.prototype, Backbone.View.prototype, {       
        bindTo: function(model, ev, callback) {

            model && model.bind(ev, callback, this);
            this.bindings.push({
                model: model,
                ev: ev,
                callback: callback
            });
        },
        unbindFromAll: function() {
            _.each(this.bindings, function(binding) {
                binding.model.unbind(binding.ev, binding.callback);
            });
            this.bindings = [];
        },
      
        close: function() {

            this.onResizeWindow && $(window).off("resize", this.onResizeWindow);
            //$('select', this.$el).selectpicker('destroy');
            //Application.Events.off(null, null, this);
            this.stopListening();
            this.unbindFromAll(); 
            this.unbind();      
          
            this.remove(); 
        },
        disposeSubViews: function(items, allItems) {

            items = allItems ? this.subViews : items;

            for (var index in items) {
                this.subViews[index].close();
                delete this.subViews[index];
            }
            ;
        }
    });

    Backbone.View = (function(View) {
        return View.extend({
            constructor: function(options) {
                this.options = options || {};
                View.apply(this, arguments);
            }
        });
    })(Backbone.View);

    _.mixin({
               
    })

    BaseView.extend = Backbone.View.extend;
    return BaseView;
});
