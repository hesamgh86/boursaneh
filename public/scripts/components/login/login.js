
$( document ).ready(function() {
    window.mobile = "";
    window.roleId = "";
    window.code = "";
    window.newPass = "";
    
    $("#login").validate({        
        rules: {
            roleId:{
                required: true,
            },
            identity:{
                required: true,
                minlength: 6,
            },
            credential:{
                required: true,
                minlength: 6,
            },
        },messages: {
            identity:{
                required: "نام کاربری را وارد کنید",
                minlength: "طول نام کاربری حداقل ۶ حرف باشد",
            },    
            credential:{
                required: "رمز عبور را وارد کنید",
                minlength: "طول رمز حداقل ۶ حرف باشد",
            },
            roleId:{
                required: "نقش خود را انتخاب کنید",
            },
        },
        errorClass: "form-invalid",    
    });

    $('.selectpicker').on('show.bs.select', function (e) {
          $('.bootstrap-select label').remove();
    });

    $( ".submit-send-sms" ).click(function() {
        var $this = this;
        $('#role-error').remove();
        $('#mobile-error').remove();
        if( $(".forgotPass #role").val() == "" ){
            $(".forgotPass .role-forgot").append('<label id="role-error" class="form-invalid" for="role">نقش خود را انتخاب کنید</label>')
        }else if( $('.forgotPass #mobile').val() == "" ){
            $('.forgotPass .mobile-wrapper').append('<label id="mobile-error" class="form-invalid" for="mobile">نام کاربری را وارد کنید</label>');
        }else if( $('#role-error').length == '0' && $('#mobile-error').length == '0' ){
            window.mobile = $('.forgotPass #mobile').val();
            window.roleId = parseInt($(".forgotPass #role").val());

            var data = { "mobile" : window.mobile, roleId : window.roleId };
            $.ajax({
                url: "http://88.99.211.43:6975/api/forgot-password",
                type : "POST",
                contentType: 'application/json',
                data : JSON.stringify(data),
                dataType: 'json',
                success: function(msg){
                    showCodeModal();
                },
                error: function(msg){
                    if( msg.responseJSON.detail.code == "701" ){
                        PNotify.prototype.options.styling = "bootstrap3";
                        new PNotify({
                            title   : "خطا",
                            text    : 'کاربری با این نام کاربری وجود ندارد',
                            type    : "error",
                            delay   : 2000,
                        }); 
                    }
                }
            });
        }
    });

    function showCodeModal(){
        $(".modal.forgotPass").modal('hide');
        $(".modal.sendCode").modal('show');
    };


    $( ".submit-code" ).click(function() {
        var $this = this;
        window.code = $(".sendCode #code").val()
        var data = { "mobile" : window.mobile , code : window.code , roleId : window.roleId  };
        $.ajax({
            url: "http://88.99.211.43:6975/api/verification/verify-code",
            type : "POST",
            contentType: 'application/json',
            data : JSON.stringify(data),
            dataType: 'json',
            success: function(msg){
               if(msg.success = true){                
                   showResetPassModal(); 
               }
            },
            error: function(msg){
               if( msg.responseJSON.detail.code == "814" ){
                PNotify.prototype.options.styling = "bootstrap3";
                new PNotify({
                    title   : "خطا",
                    text    : 'کد وارد شده صحیح نیست',
                    type    : "error",
                    delay   : 2000,
                }); 
               }
            }
        });
    });  

    function showResetPassModal(){
        $(".modal.sendCode").modal('hide');
        $(".modal.resetPass").modal('show');
    };


    $( ".submit-pass" ).click(function() {
        var $this = this;
        $('#pass-error').remove();
        $('#confirm-error').remove();
        if( $(".resetPass #pass").val() == "" ){
            $(".resetPass .pass-wrapper").append('<label id="pass-error" class="form-invalid" for="role">رمز عبور را وارد کنید</label>')
        }else if( $(".resetPass #pass").val().length < 6 ){
            $(".resetPass .pass-wrapper").append('<label id="pass-error" class="form-invalid" for="role">رمز عبور باید بیشتر از ۶ کاراکتر باشد</label>')
        }else if( $('.resetPass #confirm').val() == "" ){
            $('.resetPass .confirm-wrapper').append('<label id="confirm-error" class="form-invalid" for="mobile">تکرار رمز عبور را وارد کنید</label>');
        }else if( $('.resetPass #confirm').val() !=  $(".resetPass #pass").val() ){
            $('.resetPass .confirm-wrapper').append('<label id="confirm-error" class="form-invalid" for="mobile">تکرار رمز عبور باید با رمز عبور برابر باشد</label>');
        }else if( $('#pass-error').length == '0' && $('#confirm-error').length == '0' ){

            window.newPass = $('.resetPass #pass').val();
            var data = { "roleId" : window.roleId , mobile : window.mobile ,code : window.code , newPass : window.newPass };

            $.ajax({
                url: "http://88.99.211.43:6975/api/reset-password",
                type : "POST",
                contentType: 'application/json',
                data : JSON.stringify(data),
                dataType: 'json',
                success: function(msg){
                    PNotify.prototype.options.styling = "bootstrap3";
                    new PNotify({
                        title   : "موفقیت",
                        text    : 'با موفقیت رمز تغیر کرد',
                        type    : "success",
                        delay   : 2000,
                    });
                   $(".modal.resetPass").modal('hide');
                }
            });
        }
    });    


});



