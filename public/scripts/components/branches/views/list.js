define([
    
    'core/application',
    'core/components/base_list',    
    'collections/branches',
    'components/branches/views/form',
    'text!components/branches/tpls/list.html',

], function( Application, BaseListView, BranchesCollection, FormView, ListTpl) {
        
        var BranchesListView = BaseListView.extend({
            typeView    : 'row',
            className   : "branches-list",            
            events:{
                'click .creat-item .btn'       : 'creatItem',
            },
            
            initialize: function(options) {
                _.bindAll(this,'loadMore');

                this.status = this.options.status;
                this.mode = this.options.mode;

                this.collection = new BranchesCollection([], {
                    filter : {
                        status : this.status
                    }
                });                

                BranchesListView.__super__.initialize.call(this, this.options);
                return this;
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            getTableOptions: function(){
                var aoColumns = '', columnDefs = '';

                columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns = this.getClumens();

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },            
            getClumens: function () {
                var list = [];

                list   = 
                [   
                    { "mDataProp":  'id' },
                    { "mDataProp":  'title' },
                    { "mDataProp":  'address' },
                    { "mDataProp":  'phone' },
                    { "mDataProp":  'ivr' },
                    { "mDataProp":  'fax' },
                    { "mDataProp":  'email' },
                    { "mDataProp":  'action' },
                ];
                
                return list;
            },

            itemValue: function(model){

                var obj = {
                    'id'            : '<div class = "left-text">' + ( model.get('id') ? model.get('id') : '' ) + '</div>',
                    'title'         : '<span class="whiteNorm">' + ( model.get('title') ? model.get('title') : '' ) + '</span>',
                    'address'       : '<span class="whiteNorm">' + ( model.get('address') ? model.get('address') : '' ) + '</span>',
                    'phone'         : '<span class="whiteNorm left-text">' + ( model.get('phone') ? model.get('phone') : '' ) + '</span>',
                    'ivr'           : '<span class="whiteNorm left-text">' + ( model.get('ivr') ? model.get('ivr') : '' ) + '</span>',
                    'fax'           : '<span class="whiteNorm left-text">' + ( model.get('fax') ? model.get('fax') : '' ) + '</span>',
                    'email'         : '<span class="whiteNorm left-text">' + ( model.get('email') ? model.get('email') : '' ) + '</span>',
                    'action'        : '<a class = "show-page center" href = "#branches/'+ ( model.get('id') ? model.get('id') : '') +'"><span class="text-state-info">اطلاعات بیشتر</span></a>',
                };

                return obj;
            },

            creatItem: function(e){
                this.formView = new FormView({
                    collection: this.collection,
                    mode : 'create',
                });
                this.formView.render();
                this.formView.afterAppend(); 
            },

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    Application     : Application,
                    mode            : this.mode,
                })));

                this.afterRender();
                return this;
            },
        });
        return BranchesListView;
    });








