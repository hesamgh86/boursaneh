define([
    'jquery',
    'backbone',
    'underscore',
    'core/application',
    'core/components/base_view',    

    ], function($, Backbone, _, Application, BaseView) {
        var BaseItemView = BaseView.extend({
            mode        : 'edit',
            pageModel   : {},
            pageHolder  : '',
            typeView    : 'box',
            initialize: function(options) {

                if( this.options && this.options.model )
                    this.model = this.options.model;

                this.typeView = this.options.typeView || "box"
                this.render();
            },

            render: function(){
                var tpl = this.getTemplate();
                this.$el.html(tpl({
                    user        : Application.User.toJSON(),
                    model       : this.model.toJSON(),
                    imageUrl    : Application.URLIMAGE,
                    typeView    : this.typeView
                }));

                this.afterRender();
                return this;
            },
            afterRender: function(){

            },
            getTemplate: function(){
                return '';
            },
            
            getPageView: function(){
                return '';
            },

            showItem: function(e){
                
            },
            
            toggleBackBtn: function(){},

            toggleBackBtn: function(type){

                if( type ){
                    $('.back-to-order').css('display' , 'block');
                    $('.all-search').css('display' , 'none');    
                }
            },
            
            addMarker: function () {}
        });

        return BaseItemView;
    });
