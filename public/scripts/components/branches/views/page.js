define([
    
    'core/application',
    'core/components/base_page',   
    'core/components/base_map',   
    'models/branche',
    'text!components/branches/tpls/page.html', 
    'components/branches/views/form',    
    'bootbox',
    'lightBox',

    ], function( Application, BasePageView , MapView , Model, PageTpl, FormView,bootbox  ) {

        var PageView = BasePageView.extend({
            name      : 'branche',
            isNeedMap : true,

            initialize: function() {

                if( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }
                if( this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if( this.options && this.options.id ){
                    this.model.set({ id: this.options.id });
                }

                PageView.__super__.initialize.call(this, this.options);
            },
            
            getTemplate: function () {
                return _.template(PageTpl);
            },

            afterAppend: function(){

                this.setMapInit();
                // if( this.isNeedMap && this.model.get('location') ){
                //     this.setMapInit();
                
                // }else{
                //     $('#map-canvas').html('<div class="empty-map">آدرس روی نقشه مشخص نشده</div>')
                // }
            },

            setEdit: function(e){

                this.formView = new FormView({
                        model   : this.model,
                        mapType : this.mapType, 
                    });
                this.formView.render();
                this.formView.afterAppend();
            },
            setApprove: function () {
                var $this = this;
                var data = { status : 1 };
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        $.ajax({
                            url: Application.URL + '/api/branches/' + $this.model.get('id') + '/status',
                            type : "POST",
                            contentType: 'application/json',
                            data : JSON.stringify(data),
                            dataType: 'json',
                            success: function(data) {
                                Application.showNotify( {
                                    name: 'success',
                                    msg: 'با موفقیت تایید شد',
                                    title: '',
                                    delay : 100
                                });
                                $this.model.set({status: 1});
                                $this.render();
                                $this.afterAppend();
                            }
                        });
                    }
                });
            },            
            setReject: function () {
                var $this = this;
                var data = { status : -1 };
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        $.ajax({
                            url: Application.URL + '/api/branches/' + $this.model.get('id') + '/status',
                            type : "POST",
                            contentType: 'application/json',
                            data : JSON.stringify(data),
                            dataType: 'json',
                            success: function(data) {
                                Application.showNotify( {
                                    name: 'success',
                                    msg: 'با موفقیت عدم تایید شد',
                                    title: '',
                                    delay : 100
                                });
                                $this.model.set({status: -1});
                                $this.render();
                                $this.afterAppend();                            }
                        });
                    }
                });
            },  

            setRemove: function () {
                var $this = this;                
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        bootbox.prompt({
                            title: "دلیل حذف این ایده را در کادر زیر بنویسید :",
                            inputType: 'textarea',
                            callback: function (result) {
                                if( result ){
                                    if( result != "" ){
                                        var data = { reason : result };
                                        $.ajax({
                                            url: Application.URL + '/api/branches/' + $this.model.get('id'),
                                            type : "DELETE",
                                            contentType: 'application/json',
                                            data : JSON.stringify(data),
                                            dataType: 'json',
                                            success: function(data) {
                                                Application.showNotify( {
                                                    name: 'success',
                                                    msg: 'با موفقیت حذف شد',
                                                    title: '',
                                                    delay : 100
                                                });
                                                $this.changeViewIdeas();
                                            }
                                        });
                                    }    
                                }else{
                                    if( result == "" ){
                                        Application.showNotify( {
                                            name: 'error',
                                            msg: 'لطفا دلیل حذف این ایده را بنویسید',
                                            title: '',
                                            delay : 100
                                        });
                                        return false;
                                    }

                                }
                            },
                            buttons: {
                                confirm: {
                                    label: 'حذف',
                                },
                                cancel: {
                                    label: 'بستن',
                                }
                            },
                        });
                    }
                });
            },     

        });

        return PageView;
    });
