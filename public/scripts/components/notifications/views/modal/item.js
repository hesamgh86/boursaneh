define([

    'core/application',
    'core/components/base_item',    
    'text!components/notifications/tpls/modal/item.html',

    ], function( Application, BaseView, ItemTpl ) {
        var BaseItemView = BaseView.extend({
            tagName         : 'li',
            className       : '',
            mode            : 'edit',
            
            events:{
                'click .notify-item' : 'send'
            },

            initialize: function(options) {

                _.bindAll(this, 'onRemoveItem');
                if( this.options && this.options.model ){
                    this.model = this.options.model;
                }

                if( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }
                BaseItemView.__super__.initialize.call(this, this.options);
            },

            afterRender: function(){
                this.$el.attr({ 'data-id': this.model.get('id') });
                this.$el.append('<li class="divider"></li>')
            },

            getTemplate: function(){
                return _.template(ItemTpl);
            },
            
            send: function(e){
                
                var self = this;
                if(this.model){
                   this.model.save({}, {
                        success: function(){
                            self.onRemoveItem(self.model)
                        }
                    });
                }else{
                    return false;
                }
            },

            onRemoveItem: function(model){
                
                var navigate = '';
                if( this.model.get('refType') == 1 ){
                    navigate = '#report/' + this.model.get('refId');
                }
                if( this.model.get('refType') == 3 ){
                    navigate = '#warrant/' + this.model.get('refId');
                }
                
                this.collection.remove(this.model)
                this.model.get("clickable") && Backbone.history.navigate(navigate, true);
                this.$el.remove();

                if( this.collection.length ){
                    var count = 0;
                    _.each(this.collection.toJSON(), function(item){
                        if( item.status == 0 ){
                            count ++;
                        }
                    });

                    $('.count-info .badge-primary').html(count).removeClass('hide');

                }else{

                    $('.count-info .badge-primary').addClass("hide")
                }
            }
        });

        return BaseItemView;
    });
