define([

    'core/application',
    'core/model/base',

], function( Application, BaseModel ) {
    var Model = BaseModel.extend({
        name : "User",
        defaults : {
        },
        itemValidation: {

        },

        url : function(){
            return Application.URL + "/api/users" + (this.get("id") ? "/" + this.get("id") : "") ;
        },       

    });
    return Model;
});
