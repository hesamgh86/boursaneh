require([

    'core/application', 
    'models/notification',
    'bootbox',
    'moment', 

    ],function ( Application, NotificationModel, bootbox ) {

        (function($) {
            $('[data-toggle="tooltip"]').tooltip({
                container : 'body',
            })

            $.fn.serializeJSON = function() {
                var json = {};
                var disabled = $(this).find(':input:disabled');

                disabled.each(function(index,input){
                    $(input).removeAttr("disabled")
                })

                jQuery.map($(this).serializeArray(), function(n, i) {
                    json[n['name']] = n['value'];
                });

                $(this).find('input[type="checkbox"]').each(function() {
                    if ($(this).attr('name') && typeof json[$(this).attr('name')] != 'undefined')
                        json[$(this).attr('name')] = true;
                });

                disabled.attr("disabled", "disabled");
                return json;
            };

            /*------------------------------------------------------*/
            /**
             *  Give backbone an easier way to access super properties and methods.
             */
            Backbone.View.prototype.parent = Backbone.Model.prototype.parent = Backbone.Collection.prototype.parent = function(attribute, options) {

                /**
                 *  Call this inside of the child initialize method.  If it's a view, it will extend events also.
                 *  this.parent('inherit', this.options);  <- A views params get set to this.options
                 **/
                if (attribute == "inherit") {
                    this.parent('initialize', options); // passes this.options to the parent initialize method

                    //extends child events with parent events
                    if (this.events) {
                        $.extend(this.events, this.parent('events'));
                        this.delegateEvents();
                    }

                    return;
                }

                /**
                 *  Call other parent methods and attributes anywhere else.
                 *  this.parent('parentMethodOrOverriddenMethod', params) <- called anywhere or inside overridden method
                 *  this.parent'parentOrOverriddenAttribute') <- call anywhere
                 */
                return (_.isFunction(this.constructor.__super__[attribute])) ?
                    this.constructor.__super__[attribute].apply(this, _.rest(arguments)) :
                    this.constructor.__super__[attribute];
            };
  
            /*
             * Persian digit to English
             * Copyright(C) 2009 by Amin Akbari [ http://eAmin.me/ ]
             * Licensed under the MIT Style License [http://www.opensource.org/licenses/mit-license.php]
             *
             */
            String.prototype.toEnDigit = function() {

                return this.replace(/[\u06F0-\u06F9]+/g, function(digit) {

                    var ret = '';

                    for (var i = 0, len = digit.length; i < len; i++) {
                        ret += String.fromCharCode(digit.charCodeAt(i) - 1728);
                    }

                    return ret;
                });
            };

            String.prototype.toFaDigit = function() {
                return this.replace(/\d+/g, function(digit) {

                    if( typeof digit != 'string' ){
                        digit = digit.toString();
                    }

                    var ret = '';
                    for (var i = 0, len = digit.length; i < len; i++) {
                        ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
                    }

                    return ret;
                });
            };

            /*------------------------------------------------------*/
            $(document).click(function(e) {

                if(!$(e.target).closest('.dropdown-notifications').length) {

                    if($('.dropdown-notifications').hasClass("open")) {
                        $('.dropdown-notifications').removeClass("open");
                    }
                }

                if(!$(e.target).closest('.dropdown-issue').length) {

                    if($('.dropdown-issue').hasClass("open")) {
                        $('.dropdown-issue').removeClass("open active");
                    }
                }

                if( !$(e.target).closest('.sidenav').length && !$(e.target).closest('.toggle-nav').length){
                    if( $('body').hasClass('open-nav') ){
                        $('body').removeClass('open-nav');
                        $('.wrapper-menu-bar').removeClass('change');
                        
                    }
                }
            });
            
            $('#helpNavbar').click(function() {

                if($( window ).width() > 767){
                    IntroJs().setOptions({ 'nextLabel': 'بعدی', 'prevLabel': 'قبلی', 'skipLabel': 'خروج', 'doneLabel': 'اتمام' }).start();
                }else{
                    Backbone.history.navigate('help',{trigger : true});
                }
            });

            String.prototype.capitalize = function() {
                return this.charAt(0).toUpperCase() + this.slice(1);
            };

            moment = require('moment');

            Application.listTypeView = 'box-order';

            Application.Map = {
                barView : {},
                mapApp  : {},
            };
            
            Application.AppEvent = Backbone.Events;

            Application.dateTime = {
                removeSecond: function(a, b, c){
                    var time = moment(new Date(a)).format('H:mm');
                    return time;
                }
            };

            $(document).click(function(event) {
                if(!$(event.target).closest('.dropdown-notifications').length) {

                    if($('.dropdown-notifications').hasClass("open")) {
                        $('.dropdown-notifications').removeClass("open");
                    }
                }

                if(!$(event.target).closest('.dropdown-issue').length) {

                    if($('.dropdown-issue').hasClass("open")) {
                        $('.dropdown-issue').removeClass("open active");
                    }
                }
            });

            $('.dropdown-notifications').click(function(e){
                
                if( !$(e.currentTarget).hasClass('open') ){
                    var model = new NotificationModel({id: 0});
                    model.save();
                }
            });

            Application.PackingType = [
                                {id: 1, name: 'فاقد بسته بندی'},
                                {id: 2, name: 'بشکه'},
                                {id: 3, name: 'پاکت'},
                                {id: 4, name: 'پالت'},
                                {id: 5, name: 'جعبه'},
                                {id: 6, name: 'حلب'},
                                {id: 7, name: 'حلقه'},
                                {id: 8, name: 'دستگاه'},
                                {id: 9, name: 'راس'},
                                {id: 10, name: 'رول'},
                                {id: 11, name: 'سایر'},
                                {id: 12, name: 'شاخه'},
                                {id: 13, name: 'صندوق'},
                                {id: 14, name: 'عدل'},
                                {id: 15, name: 'فله'},
                                {id: 16, name: 'قالب'},
                                {id: 17, name: 'قرقره'},
                                {id: 18, name: 'قوطی'},
                                {id: 19, name: 'کارتن'},
                                {id: 20, name: 'کانتینر ۴۰ فوت'},
                                {id: 21, name: 'کانتینر ۲۰ فوت'},
                                {id: 22, name: 'کلاف'},
                                {id: 23, name: 'کیسه'},
                                {id: 24, name: ' کیسه جامبو'},
                                {id: 25, name: 'گونی'},
                                {id: 26, name: 'نگله'},
                                {id: 27, name: 'یخچالی'}
                            ];
            
    })(jQuery);
});


/*add         : function(e,data){
                        
                        var opt = {
                            userId              : model.get('id'),
                            avatar              : data.files[0].name,
                            nationalId          : model.get('nationalId'),
                        };

                        data.submit();
                    },*/