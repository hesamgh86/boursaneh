define([
    
    'core/application',
    'core/collection/base',
    'models/article'

], function( Application, BaseCollections, IdeaModel ) {

    var Collection = BaseCollections.extend({
        model: IdeaModel,
        name : "Articles",
        url : function(){
            return Application.URL + '/api/articles';
        },
        setParams : function(){
            this.filters.set(_.extend({}, this.params));
        },
    });

    return Collection;
});
