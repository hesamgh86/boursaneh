define([
    
    'core/application',
    'core/components/base_list',
    'collections/notifications',
    'text!components/notifications/tpls/page/list.html',
    
    ], function(Application, BaseListView, Collection, ListTpl) {

        var NotificationsList = BaseListView.extend({
            name        : 'owners',
            typeAppend  : 'append',
            className   : '',
            mode        : 'create',
            canSeePage  : false,
            events:{
                
                'click tr.hasNotBeenSeened'  : 'sendSeen',
                'click tr .show-page'        : 'showPage',
            },

            initialize: function(options) {
                _.bindAll(this, 'saveSuccess', 'navigate');

                this.collection = new Collection([]);
                NotificationsList.__super__.initialize.call(this, this.options);
                return this;
            },

            getTableOptions: function(){

                var columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns   = 
                    [
                            { "mDataProp":  'id', "width": "100px"},
                            { "mDataProp":  'type'},
                            { "mDataProp":  'text'},
                            { "mDataProp":  'date',"width": "100px"},
                            { "mDataProp":  'action', "width": "80px" },
                    ];

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },

            itemValue: function(model){

                var obj = {}, fullname = '', moreTpl = '',
                    from = model.get('from') || '';

                if( model.get("clickable") ){
                    moreTpl = '<a class = "show-page"><span class="text-state-info">اطلاعات بیشتر</span><span class="icon-state-info"><i class="glyphicon glyphicon-menu-left"></i></span></a>';
                }

                obj = {
                    'id'          : '<div class="ltr">' + model.get("refId") + '</div>',
                    'type'        : '<span>' + model.get('refText') + '</span>',
                    'text'        : '<span>' + model.get("text") + '</span>',
                    'date'        : '<div class="date-value ltr">' + Application.convertUTCDate( model.get('createdDate') ) + '</div>',
                    'action'      : moreTpl,
                };

                return obj;
            },

            afterRowRenderItem: function(model, row){

                if(model.get("status") == 0){
                    if( row ){
                        $(row).addClass('hasNotBeenSeened');
                        $(row).attr({'data-id': model.get('id')});
                    }
                }else if(model.get("status") == 1){
                    if( row ){
                        $(row).addClass('hasBeenSeened');
                        $(row).attr({'data-id': model.get('id')});
                    }
                }
            },

            getItemView: function(model){
                return '';
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            sendSeen: function(e){

                var el = $(e.target).closest("tr"),
                    id = el.data("id"),
                    model = this.collection.get(id),
                    self = this;

                if(model){
                    model.save({
                        status : 1
                    }, {
                        success: function(model){
                            self.saveSuccess(model, el);  
                        }
                    });
                }
            },

            showPage: function(e){

                var el = $(e.target).closest("tr"),
                    id = el.data("id"),
                    model = this.collection.get(id);

                this.canSeePage = true;

                if( model.get("status") == 0 ){
                    this.sendSeen(e);

                }else{
                    this.navigate(model);
                }
            },

            saveSuccess: function(model, el){

                // this.collection.remove(model);
                var unseenModel = Application.notificationsCollection.get(model.id);
                if(unseenModel){
                    Application.notificationsCollection.remove(unseenModel);
                }
                // $(".dropdown-notifications li[data-id="+ model.id +"]").removeClass("hasNotBeenSeened");
                el.removeClass("hasNotBeenSeened").addClass('hasBeenSeened');

                if( this.canSeePage ){
                    this.canSeePage = false;
                    this.navigate(model);
                }
            },

            navigate: function(model){

                var url = '';
                switch ( model.get('refType') ){
                    case 1:
                        url = '#report/' + model.get('refId');
                    break;
                    case 2:
                        url = '#idea/' + model.get('refId');
                    break;
                    case 3:
                        url = '#warrant/' + model.get('refId');
                    break;
                }

                model.get("clickable") && Backbone.history.navigate( url, true);
            },
        });

        return NotificationsList;
    });



/*


*/