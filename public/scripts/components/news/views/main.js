define([

    'core/application',
    'core/components/base_section',
    'components/news/views/list',
    'text!components/news/tpls/main.html',
    
    ], function(Application, BaseSection, ListView, MainTpl) {
        var MainView = BaseSection.extend({
            name  : 'news',
            events:{
            },
            initialize: function(options) {
                MainView.__super__.initialize.call(this, this.options);
            },
            
            beforeRender: function(){
                this.render();
            },

            afterRender: function(){
                this.createListView();
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },
            
            getListView: function(){
                return new ListView({
                    status : this.status,
                    mode : 'news',
                });
            },

            afterAppend: function(){},

        });

    return MainView;
});