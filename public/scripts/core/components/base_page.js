define([
    
    'core/application',
    'core/components/base_view',
    'core/components/base_map',
    /* define plugin */
    'bootbox',
    'pnotify',

    ], function( Application, BaseView, MapView, bootbox,PNotify ) {
        var BasePageView = BaseView.extend({
            className       : 'middle-content',
            mode            : 'edit',
            isNeedMap       : false,
            events          : {

                'click .approve'    : 'setApprove',   
                'click .reject'     : 'setReject',                
                'click .edit'       : 'setEdit',  
                'click .remove'     : 'setRemove',
            },
            
            initialize: function() {

                _.bindAll(this, 'render', 'afterRender', 'afterAppend', 'refresh');
                this.model.on('updated', this.refresh, this);
                this.beforeRender();
            },

            beforeRender: function(){
                var $this = this;
                if(this.model){
                    this.model.fetch({
                        async : false,
                        success : function(){
                            $this.render();
                        }
                    });
                }
            },
            
            render: function(){
                var tpl = this.getTemplate();
                this.$el.html(tpl({
                    Application : Application,
                    model   : this.model.toJSON(),
                    session : Application.Session.toJSON(),
                    rootUrl : Application.URL,
                    imageUrl: Application.URLIMAGE,
                }));

                this.afterRender();
                return this;
            },
            
            refresh: function(){

                this.render();
                this.afterAppend();
                $('.modal-backdrop').remove();
                $('body.modal-open').removeClass('modal-open');
            },

            getSubTemplate: function(){
                return '<div></div>';
            },

            /* suppose template*/
            getTemplate: function () {
                return '';
            },
            
            /* get item view */
            getItemView: function (model) {
                return '';
            },
            
            afterRender: function(){
                this.refreshSelectPicker();
            },
            
            afterAppend: function(){

                if( this.isNeedMap ){
                    this.setMapInit();
                }
            },

            refreshSelectPicker: function(){

                this.$('select').selectpicker('refresh'); 
            },

            createHitoriesView: function(){},

            setMapInit: function(){

                var position = this.model.get('location') || '';
                this.mainMapView = new MapView({
                    viewType : 'view',  
                    position : position,              
                });                

                this.mainMapView.render();
            },
            
            changeViewDashboard: function(){
                 Backbone.history.navigate('#dashboard' , {trigger: true});
            },
            changeViewReports: function(){
                 Backbone.history.navigate('#reports' , {trigger: true});
            },
            changeViewIdeas: function(){
                 Backbone.history.navigate('#ideas' , {trigger: true});
            },
            changeViewCitizens: function(){
                 Backbone.history.navigate('#citizens' , {trigger: true});
            },
            changeViewPolls: function(){
                 Backbone.history.navigate('#polls' , {trigger: true});
            },

            setEdit: function(){},
            setApprove: function () {},            
            setReject: function () {},  
            setRemove: function () {},   

        });
    
        return BasePageView;
});