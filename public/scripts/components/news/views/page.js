define([
    
    'core/application',
    'core/components/base_page',  
    'models/news',
    'text!components/news/tpls/page.html', 
    'components/news/views/form',    
    'bootbox',
    'lightBox',
    'highcharts',
    'highchartsExport',
    'highcharts3d',
    'highchartsMore',

    ], function( Application, BasePageView , Model, PageTpl, FormView,bootbox  ) {

        var PageView = BasePageView.extend({
            name      : 'poll',

            events :function() { 
                var parentEvents = BasePageView.prototype.events;
                if(_.isFunction(parentEvents)){
                      parentEvents = parentEvents();
                }
                return _.extend({},parentEvents,{
                    'click .close-item'     : 'setClose',
                });
            },

            initialize: function() {

                if( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }
                if( this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }

                if( this.options && this.options.id ){
                    this.model.set({ id: this.options.id });
                }

                PageView.__super__.initialize.call(this, this.options);                  

            },
                    // _data.push(answers[index]);

            renderStatisticsPolls: function(){
                var $this = this;
                var chartData = [];
                var states = this.model.get("stats").answers ||[];
                var answers = this.model.get("answers") ||[];
                _.each(states, function(opt, index){
                    chartData.push({ name : ''+  ++index , y : opt});
                })

                $('#statisticsPolls', this.$el).highcharts({
                    chart: {
                        type: 'pie',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name:'Result',
                        colorByPoint: true,
                        data:  chartData
                    }],
                });
            },
            
            getTemplate: function () {
                return _.template(PageTpl);
            },

            afterAppend: function(){
                if( this.model.get('stats') && this.model.get('stats').totalAnswers != 0 ){
                    this.renderStatisticsPolls();
                }
            },

            setEdit: function(e){

                this.formView = new FormView({
                        model   : this.model,
                    });
                this.formView.render();
                this.formView.afterAppend();
            },
           
            setRemove: function () {
                var $this = this;
                var data = {};
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        $.ajax({
                            url: Application.URL + '/api/news/' + $this.model.get('id'),
                            type : "DELETE",
                            contentType: 'application/json',
                            data : JSON.stringify(data),
                            dataType: 'json',
                            success: function(data) {
                                Application.showNotify( {
                                    name: 'success',
                                    msg: 'با موفقیت حذف شد',
                                    title: '',
                                    delay : 100
                                });
                                $this.changeViewPolls();
                            }
                        });
                    }
                });
            }, 
            setClose: function () {
                var $this = this;
                var data = { status : -1 };
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        $.ajax({
                            url: Application.URL + '/api/news/' + $this.model.get('id') + '/close',
                            type : "POST",
                            contentType: 'application/json',
                            data : JSON.stringify(data),
                            dataType: 'json',
                            success: function(data) {
                                Application.showNotify( {
                                    name: 'success',
                                    msg: 'با موفقیت بسته شد',
                                    title: '',
                                    delay : 100
                                });
                                $this.changeViewPolls();
                            }
                        });
                    }
                });
            }, 
        });

        return PageView;
    });
