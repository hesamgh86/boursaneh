define([
    'jquery',
    'backbone',
    'underscore',
    'core/application',
    'core/components/base_view',
    'datatables.net',
    'datatablebootstrap',
    'datatablebuttons',
    'datatableselect',

    ], function($, Backbone, _, Application, BaseView ) {
        var BaseListView = BaseView.extend({
            items           : [],
            holder          : '.list-holder',
            typeAppend      : 'append',
            // typeView        : 'row',
            hasTable        : true,
            mapViewRef      : null,
            markers         : [],

            initialize: function() {
                _.bindAll(this, 'itemValue', 'showAllLocations');

                if(this.collection){
                    this.collection.setFilters(this.options.collectionFilter || {});
                    this.collection.on('add', this.addNewItem, this);
                    this.collection.on('reset',this.reset, this);
                    this.collection.on('beforeFetch',this.beforeFetch, this);
                    this.collection.on('fetchSuccess', this.renderItems, this);
                    this.collection.on('change', this.onChangeItem, this);
                    this.collection.on('remove', this.onRemoveItem, this);
                }
                this.fetchCollection = ("fetchCollection" in this.options) ? this.options.fetchCollection : true
                this.itemTpl = "";
                this.typeView = this.options.typeView || "row";
                this.beforeRender();
                this.render();
                if(this.mapViewRef){
                    Application.AppEvent.on('map:is:loaded', this.showAllLocations, this);
                }


            },

            /* fetch collection*/
            beforeRender: function(){
                if ( this.fetchCollection ) {
                    this.collection.fetch();
                }
            },

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    isBoxViewMode   : this.typeView == "box" ? true : false,
                    Application     : Application
                })));

                this.afterRender();
                return this;
            },

            afterRender: function(){
                if(this.hasTable){
                    this.initTable();
                }
            },

            itemValue: function(model){
                return {};
            },

            getTableOptions: function(){
                return {
                    searching       : false,
                    lengthChange    : false,
                    ordering        : false,
                }
            },

            getClumens : function(){},

            initTable:function(){
               var opt = this.getTableOptions() || {}

               this.table = $("table", this.$el).DataTable(_.extend({

                    "bAutoWidth"    : false,
                    "aoColumns"     : this.getClumens() || [],
                    "columnDefs"    :  [{
                        orderable: false,
                        targets: -1
                    }],
                    "language"      : {
                        // "emptyTable": "<span class = 'no-item'>موردی یافت نشد</span>"
                    },
                    "searching"     : true,
                    "lengthChange"  : true,
                    "ordering"       : true,

                }, opt));

            },
            /* suppose template*/
            getTemplate: function () {
                return '';
            },

            /* get item view */
            getItemView: function (model) {
                return '';
            },

            reset: function (a,b,c) {

                if(this.hasTable && this.table){
                    this.table.clear().draw();
                }else{
                    $(this.holder, this.$el).empty();
                }
                if(this.mapViewRef && this.markers.length){
                    this.mapViewRef.clearMarkers(this.markers);
                    this.markers = [];
                }
            },

            beforeFetch: function(){

                if(!this.collection.length){
                    if(this.hasTable){
                        this.$('table tbody tr td').html('<div class="loading"><div><div class="c1"></div><div class="c2"></div><div class="c3"></div><div class="c4"></div></div><span>لطفا صبر کنید</span></div>');
                    }else{
                        $(this.holder, this.$el).html('<div class="loading"><div><div class="c1"></div><div class="c2"></div><div class="c3"></div><div class="c4"></div></div><span>لطفا صبر کنید</span></div>');
                    }
                }

            },

            renderItems: function(resp, collection) {

                var collection = collection || this.collection;

                if( collection.length > 0 && this.table ){
                    this.table.clear().draw();
                }
                var $this = this;
                this.beforeRenderItems(collection);

                var start = 0
                if (this.collection.filters.get('offset') > 0 ) {
                    start = this.collection.filters.get('offset')
                }

                for (var i = start; i < this.collection.length; i++) {
                    var model = this.collection.models[i];
                    $this.renderItem(model);
                    $this.mapViewRef && $this.mapViewRef.loaded && $this.showLocation(model);
                }

                this.afterRenderItems();
                this.bindingScripts();
            },

            showAllLocations: function(){
                if(this.showItemsOnMap){
                    var $this = this;
                    this.mapViewRef && this.mapViewRef.clearMarkers();
                    this.markers = [];
                    this.collection.each(function(model){
                        $this.showLocation(model);
                    });
                }
            },

            showLocation: function(){

            },
            
            renderItem: function(model){                
                if(this.hasTable){
                    this.renderRow(model);
                }else{
                    this.renderBox(model);
                }

                this.afterRenderItem(model);
            },
            renderBox: function(model) {
                var el = this.getItemTpl(model);
                $(this.holder, this.$el)[$.trim(this.typeAppend)](el);
            },

            renderRow: function(model) {
                var obj = this.itemValue(model);
                var row = this.table.row.add( obj ).order([]).draw().node();

                if( row ){
                    $(row).attr({ 'data-id': model.get('id') });
                }

                if( model.get('isAdd') ){
                    $(row).addClass('newNode');
                    setTimeout(function(){ $(row).removeClass('newNode'); }, 3000);
                }

                if( model.get('isVerified') == 0 ){
                    this.checkVerify(model, $(row) );
                }

                if( this.afterRowRenderItem ){
                   this.afterRowRenderItem(model, row);
                }
            },
            afterRowRenderItem : function(model, row){},
            afterRenderItem : function(model){},

            checkVerify: function(model, row){

                switch( model.get('action')){
                    case 1:
                        row.addClass('action-sucess');
                        break;
                    case 2:
                        row.addClass('action-warning');
                        break;
                    case 3:
                        row.addClass('action-error');
                        break;
                }
            },

            getItemTpl: function(model){
                var tpl = _.template(this.itemTpl),
                    el = tpl(_.extend(this.options,{
                        model           : model.toJSON(),
                        collection      : this.collection,
                        isBoxViewMode   : this.typeView == "box",
                        Application     : Application
                    }));

                return el;
            },

            addNewItem: function(model,collection,event){
                $(".no-data", this.$el).hide();
                if( event && event.typeAppend ){
                    this.typeAppend = event.typeAppend;
                }else{
                    this.typeAppend = 'prepend';
                }

                this.renderItem(model);

                if(this.hasTable){
                    var currentPage = this.table.page();

                    //move added row to desired index
                    var rowCount = this.table.data().length-1,
                        insertedRow = this.table.row(rowCount).data(),
                        tempRow = '';

                    for ( var i = rowCount; i >= 1; i-- ) {
                        tempRow = this.table.row(i-1).data();
                        this.table.row(i).data(tempRow);
                        this.table.row(i-1).data(insertedRow);
                    }

                    this.table.page(currentPage).draw(false);
                }
            },

            beforeRenderItems: function(collection){

                if( $('.loading')){
                    $('.loading').remove();
                }

                if( this.collection.filters.attributes.offset == 0 ){
                    $(this.holder, this.$el).empty();
                }
                if( collection.totalCount == 0 || collection.length == 0 ){                    
                    this.noItemView();
                }
            },

            afterRenderItems: function() {
                if( this.collection.length == 0 && this.table ){
                    this.table.clear().draw();
                    this.noItemView();
                }
            },

            noItemView: function(){
                var tpl = _.template("<div class='no-data'>آیتمی موجود نیست</div>");
                if(this.typeView == "row"){
                    this.$('table tbody tr td').html(tpl());
                }else{
                    $(this.holder, this.$el).html(tpl());
                }
            },

            loadMore: function() {
                if(this.collection.models.length == 0 ||
                    (this.collection.filters.attributes.offset + 1) >= this.collection.totalCount){
                    return false;
                }

                this.collection.increase().fetch({
                    add     : true,
                });
            },

            disposeAllItems : function(){
                for(var index in this.items){
                    this.items[index].remove();
                };
            },
            search: function(filter){
                var filter = filter || {};
                this.collection.reset();
                this.collection.setFilters(filter);
                this.collection.fetch();
            },

            resetPagination : function(){

            },

            updatePaging: function() {

            },
            changeRole: function(e){

                var el = $(e.currentTarget);
                if( el.hasClass('active') ){
                    el.removeClass('active');
                }else{
                    el.addClass('active');
                }


                var type = this.$(e.currentTarget).data('type');

                if(type){
                    this.collection.filters.set({type : type});
                    this.collection.fetch();
                }
            },
            onChangeItem: function(model){
                if(this.hasTable){
                    var index =  this.collection.indexOf(model);
                    var obj = this.itemValue(model);
                    this.table.row(index).data(obj).draw()
                }
            },
            onRemoveItem: function(model){
                $("[data-id"+ model.id +"]", this.$el).remove();
                if(!this.collection.length){
                    $(".no-data", this.$el).show();
                }
            }
        });

        _.mixin({

        });

        return BaseListView;
});
