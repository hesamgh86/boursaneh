define([
    
    'core/application',
    'core/components/base_list',    
    'collections/users',
    'text!components/users/tpls/list.html',

], function( Application, BaseListView, UserCollection, ListTpl) {
        
        var UserListView = BaseListView.extend({
            typeView    : 'row',
            className   : "users-list",            
            events:{
                'click .toggle-wrapper .btn'   : 'toggleStatus',
            },
            
            initialize: function(options) {
                _.bindAll(this,'loadMore');

                this.status = this.options.status;
                this.mode = this.options.mode;

                this.collection = new UserCollection([], {
                    filter : {
                        status : this.status
                    }
                });
 

                UserListView.__super__.initialize.call(this, this.options);
                return this;
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            getTableOptions: function(){
                var aoColumns = '', columnDefs = '';

                columnDefs  = [{ orderable: false, targets: -1 }];
                aoColumns = this.getClumens();

                return {
                    aoColumns   : aoColumns,
                    columnDefs  : columnDefs,
                }
            },            

            getClumens: function () {
                
                var list = [];

                list   = 
                [   
                    { "mDataProp":  'id' },
                    { "mDataProp":  'username' },
                    { "mDataProp":  'name' },
                    { "mDataProp":  'mobile' },
                    { "mDataProp":  'frozen' },
                    { "mDataProp":  'favourite' },                    
                    { "mDataProp":  'action', "width": "80px"},
                ];

                
                return list;
            },

            itemValue: function(model){

                var frozen = '', district = '';

                if( model.get('frozen') == true ){
                    frozen = 'فعال';
                }else if( model.get('frozen') == false ){
                    frozen = 'غیر فعال';
                }
                   


                var obj = {
                    'id'           : '<div class = "left-text">' + ( model.get('id') ? model.get('id') : '' ) + '</div>',
                    'username'     : '<span class="left-text">' + ( model.get('username') ? model.get('username')  : '' )+ '</span>',
                    'name'         : '<span class="left-text">' + ( model.get('firstName') ? model.get('firstName')  : '-' ) + ( model.get('lastName') ? model.get('lastName')  : '' ) + '</span>',
                    'mobile'       : '<span class="left-text">' + ( model.get('mobile') ? model.get('mobile')  : '' ) + '</span>',
                    'frozen'       : '<div class = "center">' + frozen + '</div>',
                    'favourite'    : '<div class = "center">' + ( model.get('favourite') ? model.get('favourite')  : '' ) + '</div>',
                    'action'       : '<a class = "show-page center" href = "#users/'+ ( model.get('id') ? model.get('id') : '') +'"><span class="text-state-info">اطلاعات بیشتر</span></a>',
                };

                return obj;
            },

            toggleStatus: function(e){
                var el = $(e.target).closest(".btn"),
                    parent = $(e.target).parents(".toggle-status");

                el.toggleClass("active");

                var status = [];

                if($(".btn.active",parent).length){
                    $(".btn.active",parent).each(function(index, el){
                        status.push($(el).data("id"));
                    });
                    this.search({
                        status : status
                    });
                }else{
                    this.collection.reset();
                    this.noItemView();
                }

            }, 

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    Application     : Application,
                    mode            : this.mode,
                })));

                this.afterRender();
                return this;
            },



        });

        return UserListView;
    });
