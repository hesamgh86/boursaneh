define([

    'core/application', 
    'core/components/base_view',    
    'core/components/base_map',

    /* define plugin */
    'fileupload',
    'bootbox',

    ], function( Application, BaseView, MapView,Fileupload, bootbox) {
        var BaseFormView = BaseView.extend({
            mode            : 'create',
            typeAppend      : 'append',
            modalHolder     : "#myModal",
            isNeedMap       : false,
            isNeedUpload    : false,
            showModal       : true,
            events          : {
                'click .btn-submit' : 'submit',
            },

            initialize: function() {

                _.bindAll(this, 'checkModelIsValid')

                if( this.options && this.options.mapHolderName ){
                    this.mapHolderName = this.options.mapHolderName;
                }
                this.beforeRender();
            },

            beforeRender: function(){},
            
            render: function(){

                var template = this.getTemplate();
                this.$el.html(template({
                    session         : Application.Session.toJSON(),
                    model           : this.model && this.model.toJSON(),
                    Application     : Application,                    
                }));

                if( this.afterRender ){
                    this.afterRender();
                }

                return this;
            },

            afterRender: function () {

                if(this.showModal){
                    this.showModal();
                }
                this.bindingScripts();
            },

            getTemplate: function(){
                return '<div></div>';
            },

            showModal: function(){
                $(this.modalHolder).html(this.$el).modal({
                    show        : true,
                    backdrop    : 'static',
                });
            },

            hideModal: function () {
                $(this.$el).closest(".modal").modal('hide');
                $('.modal-backdrop').remove();
                this.$el.remove();
            },

            afterAppend: function(){
                if( this.isNeedMap ){
                    this.setMapInit();
                }
                if( this.isNeedUpload ){
                    this.initUploader();
                }
                BaseFormView.__super__.afterAppend.call(this, this.options);
            },

            upload: function(){

                Application.upload(this.model);
            },

            setMapInit: function(){
                /* set map el*/
                var position = this.model.get('location') || '';
                this.mainMapView = new MapView({
                    viewType        : 'edit', 
                    position        : position, 
                    mapHolderName   : this.mapHolderName,
                });                

                this.mainMapView.render();

                if( this.mainMapView && this.mainMapView.map ) {
                    this.map = this.mainMapView.map;
                }
            },

            close: function() {

                $('#myModal').modal('hide');
                this.unbind();
                this.remove();
            },

            getCurrentModel: function (obj) {
                return {};
            },

            getObject: function(){

                var position = '', obj = this.$('form').serializeJSON();
                // $.each( obj, function( key, value ) {
                //    if( value == '' || value == undefined  ){
                //         delete obj[key]
                //    }
                // });

                if( this.mainMapView && this.mainMapView.map ){
                    position = this.mainMapView.getPosition();

                    if( position.latitude && position.longitude ){
                        obj.location = position;
                    }
                }
                if( obj.id ){
                    obj.id = parseInt( obj.id );
                }

                return obj;
            },

            modifyModel: function(obj){

                this.model.set(obj, {silent: true});
            },

            changeToEnNumber: function(){
            
                var list = this.$('.numberInput');
                
                _.each(list, function(item){
                    
                    var val = $(item).val(),
                        res = '';
                    if( val ){
                        res = val.toEnDigit();
                    }
                    $(item).val(res);
                });
            },

            submit: function(e){
                var self = this, el = $(e.target), obj = {},
                    isNew = this.model.get("id") ? false : true;

                if (e && e.preventDefault)
                    e.preventDefault();

                var eList = this.$('.has-error');
                _.each(eList, function(el){
                    $(el).removeClass('has-error');
                }); 

                if(el.hasClass("disabled")){
                    return;
                }

                el.addClass('disabled');
                this.changeToEnNumber();
                obj = this.getObject();

                if( !this.checkModelIsValid( obj ) ){
                    $('.btn-submit', this.$el).removeClass('disabled');
                    return;
                }

                if( this.modifyModel ){
                    this.modifyModel(obj);
                }            
                
                this.model.save({}, {
                    success: function(model, resp) {
                        self.saveSuccess(model);
                        if(self.collection && isNew){
                            self.collection.add(model);
                        }
                    },
                    error: function(model, err){
                         $('.btn-submit', self.$el).removeClass('disabled');
                         try {
                             err = JSON.parse(err.responseText).error;
                             self.errors(err);
                         } catch (e) {
                             self.errors([err.responseText]);
                         }
                     }
                });
            },

            errors: function(e){
                Application.showNotify( {
                    name: 'error',
                    msg: 'مشکل پیش امده',
                    title: 'خطا'
                });

            },

            reset: function() {
                this.$(':input','form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
            },

            saveSuccess: function(model, callback){

                Application.showNotify( {
                    name: 'success',
                    msg: 'با موفقیت انجام شد.',
                    title: 'موفقیت'
                });

                if(this.showModal){
                    this.hideModal();
                }

                if ( model.trigger ){
                    model.trigger('updated', model);
                }

                if (callback && typeof callback == "function") {
                    callback(model);
                }
                this.options.onSaveSuccess && this.options.onSaveSuccess(model)
            },

            checkModelIsValid: function( obj ){ 
                if ( this.model ) {
                    
                    this.model.set(obj, { silent: true});
                    var model = this.model.clone(),
                        valid = this.validateModel(model);

                    if ( !valid ){
                        return false;
                    }

                    return true;
                }

                return false;
            },

            validateModel: function(model){
                _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

                if (model.itemValidation) {
                    var _itemValidation = typeof model.itemValidation == 'function' ? model.itemValidation() :  model.itemValidation
                    model.validation = _.extend((model.validation || {}), _itemValidation);
                }

                _.extend(model, Backbone.Validation.mixin);

                if (errors = model.validate()) {
                    Application.showNotify({
                        name: 'error',
                        msg: 'لطفا اطلاعات هر بخش را کامل وارد کنید',
                        title: 'خطا'
                    });

                    $('[type=button].disabled', this.$el).removeClass('disabled')
                    this.inValid(errors, model);
                    return false;
                }

                return true;
            },

            inValid: function(errObj, error){

                var self = this, errs = [], el = '', tpl = '', val = '';
                
                $('.disabled', this.$el).removeClass('disabled');
                errs = [errObj];
                for( var i =0; i < errs.length; i++ ){

                    if( typeof errs[i] == "object" ){

                        val = errs[i];
                        for (var key in val){
                            el = this.$('[name='+key+']');
                            el.closest('.input-cnt').addClass('has-error');
                        }
                    }
                }
            },
        });

        return BaseFormView;
    });
