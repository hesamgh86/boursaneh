define([

    'core/application',
    'core/components/base_view',
    'models/asset',
    'models/categorie',
    ], function(Application, BaseView, AssetModel, CategorieModel) {
        var MainView = BaseView.extend({
            mode        : 'create',
            listHolder  : '.list-holder',
            events      :{
                'click .btn-show-form'     : 'showForm',  
            },

            initialize: function(options) {

                _.bindAll(this, 'beforeRender', 'render', 'afterRender',
                    'showForm', 'getListView' );

                this.options = options || {};

                if( this.options && this.options.listHolder ){
                    this.listHolder = this.options.listHolder;
                }

                this.beforeRender();
            },

            beforeRender: function(){

                var $this = this;
                if(this.model){
                    this.model.fetch({
                        async : false
                    });
                }

                this.render();
            },
            
            render: function(){

                var tpl = this.getTemplate();
                this.$el.html(tpl({
                    Application : Application
                }));
                this.afterRender();
                return this;
            },

            afterRender: function(){

                if( this.createListView ){
                    this.createListView();
                }
            },
            
            afterAppend: function(){
               this.renderSubViews && this.renderSubViews();
            },
            
            createListView: function(){

                if( this.options && this.options.listView ){
                    this.listView = this.options.listView;
                }else{
                    this.listView = this.getListView();
                };

                this.$(this.listHolder).html(this.listView.$el);
                this.listView.afterAppend();
                this.bindingScripts();
            },

            getListView: function(){
                return {};
            },

            showForm: function(){
            }
        });

        return MainView;
    });

