define([

    'core/application',
    'core/components/base_section',
    'components/users/views/list',
    'text!components/users/tpls/main.html',
    
    ], function(Application, BaseSection, ListView, MainTpl) {
        var MainView = BaseSection.extend({
            name        : 'users',

            events:{
            },
            initialize: function(options) {
                MainView.__super__.initialize.call(this, this.options);
            },
            
            beforeRender: function(){

                this.render();
            },

            afterRender: function(){
                this.createListView();
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },
            
            getListView: function(){
                return new ListView({
                    status : this.status,
                    mode : 'users',
                });
            },

            afterAppend: function(){},

        });

    return MainView;
});