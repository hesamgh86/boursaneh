define([

    'core/application',
    'core/components/base_form',  
    'models/article',
    'text!components/articles/tpls/form.html',

    ], function( Application, BaseFormView, Model, FormTemplate ){
        
        var FormView = BaseFormView.extend({
            className       : 'modal-dialog modal-lg',
            name            : 'location',
            mode            : 'create',
            isNeedMap       : true,
            mapHolderName   : 'map-canvas-form',
            markerChangeByProvincePosition: true,
            participationHide : false,

            initialize: function(options) {
                
                if(this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }
                
                FormView.__super__.initialize.call(this, this.options);
            },

            getTemplate: function(){

                return _.template(FormTemplate);
            },
            afterAppend: function(){   
                
                this.fillCategorySelect();  
                BaseFormView.__super__.afterAppend.call(this, this.options);

            },

            fillCategorySelect: function(){

                var list = [], id = '', self = this;
                _.each(Application.CategoriesCollection.toJSON(), function(item){
                    list.push(item);
                })
                
                if( !this.model.isNew() && this.model.get('categoryId') ){
                    if( this.model.get('categoryId') ){
                        id = this.model.get('categoryId');
                    }
                }

                var $select = this.$('select.category');
                this.appendItems(list, $select, id);
            },

            changePersianToMiladi: function(value){

                var array = value.split('/');
                var d = persianDate([parseInt(array[0]), parseInt(array[1]), parseInt(array[2])]).gDate;
                return moment(d).unix() * 1000;
            },

            getObject: function(){

                var obj = this.$('form').serializeJSON();

                obj.categoryId = parseInt(obj.categoryId);
                var publishDate = $("input.publish-date", this.$el).val();
                obj.publishDate = this.changePersianToMiladi(publishDate);

                obj.writtenBy = 1;
                obj.confirmedBy = 1;
                obj.deleted = false;
                return obj;
            },            
        });

        return FormView;
    });
