define([

    'core/application',
    'core/collection/base',
    'models/categorie'

], function( Application, BaseCollections, CategorieModel ) {

    var Collection = BaseCollections.extend({
        
        model: CategorieModel,      
        url : function(){           
            return this.baseUrl + '/api/categories';
        },
    });

    return Collection;
});
