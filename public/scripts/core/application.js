define([
    'backbone',
    'underscore',
    'jquery',
    'models/asset',
    'collections/categories',
    'pnotify',
    'bootbox'

], function(Backbone, _,$, AssetModel, CategoryCollection, PNotify, bootbox ) {

    $('#logout').on('click', function() {
        $.ajax({
            url: "/api/auth/logout",
            type : "POST",
            success: function(data) {
                window.location.href = '/login';
            }
        });
    });

    if(navigator.platform.toUpperCase().indexOf('MAC')>=0)
    $('body').addClass('ios') ;

    var role = 'operator';
    if( session.roleId == 100 ){
        var role = 'admin'
    }  
    $('body').addClass(role);

    // Redirect expired session request by ajax requests
    $(document).ajaxComplete(function (event, XMLHttpRequest, ajaxOptions) {
        try {
            var result = $.parseJSON(XMLHttpRequest.responseText);
            if (XMLHttpRequest.status == "401") {
                window.location.href = '/login'
            }
        } catch (e) {
        }
    });

    String.prototype.toEnglishDigits = function () {
        var num_dic = {
            '۰': '0',
            '۱': '1',
            '۲': '2',
            '۳': '3',
            '۴': '4',
            '۵': '5',
            '۶': '6',
            '۷': '7',
            '۸': '8',
            '۹': '9',
        }

        return this.replace(/[۰-۹]/g, function (w) {
            return num_dic[w]
        })

        //return parseInt(this.replace(/[۰-۹]/g, function (w) {
        //    return num_dic[w]
        //}));
    }


    var application = function() {
        return {
            Roles:{
                "admin"           : 100,
                "operator"        : 200,
            },
            
            
            URL         : 'http://195.201.20.42:9003',
            URLIMAGE    : 'http://195.201.20.42:9003/upfiles/',  
            

            // URL         : window.baseUrl,
            // URLIMAGE    : window.baseUrl + '/upfiles/', 

            Current_Section: "",

            User: null,
            AssetModel: new AssetModel(),
            CategoriesCollection: new CategoryCollection(),
            getPrefrences: function(){
                this.getUser();
                this.getAssets();
                this.getCategories();
            },
            
            getAssets: function(){                
                var $this = this;
                this.AssetModel.baseUrl = this.URL;
                if(!this.AssetModel.get("id")){
                    this.AssetModel.fetch({
                        async : false,
                        success : function(model, response){
                             
                        }
                    });
                }
            },
            getCategories: function(){
                var $this = this;
                this.CategoriesCollection.baseUrl = this.URL;
                if(!this.CategoriesCollection.get("id")){
                    this.CategoriesCollection.fetch({
                        async : false,
                        success : function(model, response){
                        }
                    });
                }
            },            
            getLangs: function(){
               
            },
            getUser : function(){
                this.Session.fetch({
                    async : false,
                })
            },
            ajaxsetup: function() {
                var $this = this;
                $.ajaxSetup({
                    beforeSend: function(jqXHR, settings) {
                        // jqXHR.setRequestHeader('Authorization', 'Bearer 001bed113f3dd46831562d0b359a28152a86c447');    // admin
                        // jqXHR.setRequestHeader('Authorization', 'Bearer 8fb0acf50ce09b1e35fa1d8369924e0e73a712f3');    // operator
                        // jqXHR.setRequestHeader('Authorization', 'Bearer 8640fbe8eb4c75a017cf1cd4257e0bc14b9d7f5d');    // execution
                        // jqXHR.setRequestHeader('Authorization', 'Bearer 8928695d01516f7a9ad67952c6084311907e79fd');    // contractors
                        
                        return true;
                    }
                });

                $(document).bind({
                    ajaxStart: function(request, re) {
                        //TODO: show loading
                        $('[type=button]:not(.filter)').prop("disabled", true);
                    },
                    ajaxStop: function() {
                        $('[type=button]').prop("disabled", false);
                    },
                    ajaxSuccess: function(){
                        $('[type=button]').prop("disabled", false);
                    },
                    ajaxError: function(e, error, obj) {
                        $('[type=button]').prop("disabled", false);

                        // if($.type(error.responseText) == "string"){
                        //     error = JSON.parse(error.responseText);
                        // }

                        // if(error.status == 404){
                        //     $this.showNotify( {
                        //         name: 'error',
                        //         msg: 'موردی یافت نشد.',
                        //         title: 'خطا',
                        //         delay : 100
                        //     });
                        //     Backbone.history.navigate("dashboard", true);
                        //     return
                        // }

                        // if(error.status == 403){
                        //     $this.showNotify( {
                        //         name: 'error',
                        //         msg: 'مجوز انجام این درخواست وجود ندارد',
                        //         title: 'خطا',
                        //         delay : 100
                        //     });
                        // }

                        // if(error.status == 409){
                        //     if( error.detail.code == '916' ){
                        //         $this.showNotify( {
                        //             name: 'error',
                        //             msg: 'راننده دیگری با این کد هوشمند ثبت شده است',
                        //             title: 'خطا',
                        //             delay : 100
                        //         });
                        //     }
                        //     if( error.detail.code == '917' ){
                        //         $this.showNotify( {
                        //             name: 'error',
                        //             msg: 'کامیونی با این شماره پلاک ثبت شده است',
                        //             title: 'خطا',
                        //             delay : 100
                        //         });
                        //     }
                        //     if( error.detail.code == '918' ){
                        //         _.each( $('[name=intelligentId]') , function(item){
                        //             $(item).closest('.col-md-6').addClass('has-error');
                        //         });
                        //         $this.showNotify( {
                        //             name: 'error',
                        //             msg: 'کامیون با این کارت هوشمند در سیستم موجود است',
                        //             title: 'خطا',
                        //             delay : 100
                        //         });
                        //     }

                        // }
                    }
                });
            },
            convertUTCDate: function(date, format){
                if(date && date != ""){
                    var dateString = date.replace(/-/g, "/");    
                    dateString = dateString.replace(/T/g, " ");                
                    var format = format || "YYYY/MM/DD H:m";
                    var date = new Date(dateString);
                    return persianDate(date.getTime()).format(format);
                }
                return ""
            },
            convertDate: function(date, format){debugger;
                if(date && date != ""){
                    var dateString = date.replace(/-/g, "/");
                    dateString = dateString.replace(/T/g, " ");
                    var format = format || "YYYY/MM/DD";
                    var date = this.fromUTC(dateString);
                    date = moment(date).format("YYYY/MM/DD HH:mm")

                    return this.convertUTCDate(date, format);
                }
                return ""
            },
            showNotify: function(opt){
                var time = opt && opt.waitTime ? opt.waitTime : 2000;

                PNotify.prototype.options.styling = "bootstrap3";
                new PNotify({
                    title   : opt.title || "",
                    text    : opt.msg || "",
                    type    : opt.name || "",
                    delay   : time,
                });
            },
            toUTC: function(date) {
                if(date && date!= ""){
                    var dateString = date.replace(/-/g, "/");
                    dateString = dateString.replace(/T/g, " ");
                    var date = new Date(dateString);
                    var date_utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                    return moment(date_utc).format("YYYY-MM-DD HH:mm:ss");
                }
                else{
                    return date;
                }
            },
            fromUTC: function(date) {
                if(date && date!= ""){
                    var dateString = date.replace(/-/g, "/");
                    dateString = dateString.replace(/T/g, " ");
                    var d = new Date(dateString);
                    var n = d.getTimezoneOffset();
                    var real_time = new Date(d).getTime() + -1 * n * 60 * 1000;

                    return moment(real_time).format("YYYY/MM/DD HH:mm:ss");
                }
                else{
                    return date;
                }                    
            },
            getDate: function(date){

            },
            getTime: function(date){

            },
            deleteFile : function(data, callback){
                var $this = this;
                bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                    if(result == true){
                        $.ajax({
                            url: $this.URL + "/api/delete-file",
                            type : "POST",
                            contentType: 'application/json',
                            data : JSON.stringify(data),
                            dataType: 'json',
                            success: function(data) {
                                $this.showNotify( {
                                    name: 'success',
                                    msg: 'با موفقیت حذف شد',
                                    title: '',
                                    delay : 100
                                });
                                callback && callback();
                            }
                        });
                    }
                });
            }
        }
    }
    return application();
});
