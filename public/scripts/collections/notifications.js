define([

    'core/application',
    'core/collection/base',
    'models/notification'

], function( Application, BaseCollections, Model ) {

    var Collection = BaseCollections.extend({
        model: Model,
        url : function(){
            return Application.URL + '/api/notifications';
        },
     
    });

    return Collection;
});