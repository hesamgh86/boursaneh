define([

    'core/application',
    'core/model/base',

], function( Application, BaseModel ) {
    var Model = BaseModel.extend({
        name : "Branche",
        defaults : {
        },
        itemValidation: {
            title:{
                required: true,
            },
        },
       
        url : function(){
            return Application.URL + "/api/branches" + (this.get("id") ? "/" + this.get("id") : "") + ".json";
        },
    });
    return Model;
});
