define([
    
    'core/application',
    'core/components/base_list',    
    'collections/articles',  
    'models/article',    
    'components/articles/views/form',
    'text!components/articles/tpls/date_list.html',
    'text!components/articles/tpls/date_row.html',

    /* plugins */
    'moment',
    'persiandate',
    'bootbox',

], function( Application, BaseListView, Collection, Model , FormView, ListTpl, RowTpl, moment, PersianDate, bootbox) {
        
        var TaskListView = BaseListView.extend({
            typeView    : 'row',   
            hasTable    : false, 
            holder      : '.list-holder-det',
            className   : '',  
            events:{
                'click .btn-edit' : 'edit',
                'click .btn-del' : 'delete',
            }, 
            
            initialize: function(options) {

                this.status = this.options.status;
                this.mode = this.options.mode;
                this.collection = new Collection();
                this.collection.filters.set({ dateStr: this.options.dateStr });
                TaskListView.__super__.initialize.call(this, this.options);
                return this;
            },

            beforeRender: function(){
                this.collection.fetch();
            },

            render: function(){
                if(this.typeView == "box"){
                    this.hasTable = false
                }

                var tpl = this.getTemplate();
                this.$el.html(tpl(_.extend(this.options, {
                    Application     : Application,
                    mode            : this.mode,
                })));

                this.afterRender();
                return this;
            },

            getTemplate: function () {
                return _.template(ListTpl);
            },

            renderBox: function(model) {
                var el = this.getItemTpl(model);
                $(this.holder, this.$el)[$.trim(this.typeAppend)](el);
            },

            getItemTpl: function(model){
            
                var tpl = _.template( RowTpl ),
                    el = tpl(_.extend(this.options,{
                        model           : model.toJSON(),
                        collection      : this.collection,
                        isBoxViewMode   : this.typeView == "box",
                        Application     : Application
                    }));

                return el;
            },

            edit: function(e){
                
                var id = $(e.currentTarget).closest('.article-row').attr('id');
                var model = this.collection.get(id);
                this.form = new FormView({
                    model: model
                });
                this.form.render();
                this.form.afterAppend();
            },

            delete: function(e){
                
                var self = this;
                var id = $(e.currentTarget).closest('.article-row').attr('id');
                var model = this.collection.get(id);
                
                model.destroy({success: function(){
                }});
            },
        });
        return TaskListView;
    });
