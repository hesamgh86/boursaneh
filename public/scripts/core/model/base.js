define([

    'core/application',
    'validation',
    'bootbox'

], function( Application, Validation, bootbox) {
        'use strict';
    var BaseModel = Backbone.Model.extend({
        customRemove: false,
        defaults: {
            limit: 0,
            offset: 0
        },
        dateFields : ["loadingDate", "dischargeDate", "createdDate"],
        itemValidation:{},

        validateDate: function(value, attr){
                
            var format = /^(13)\d{2}\/(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])$/;
            if( value && !format.test(value) ){
                return "format is not correct.";
            }              
        },

        destroy: function (options) {
            var $this = this;
            bootbox.confirm("آیا اطمینان دارید؟", function(result) {
                if(result == true){
                    Backbone.Model.prototype.destroy.call($this, options);
                }
            });
        },
        validateTime: function(value, attr){
              
            var format = /^((0\d|1\d|2[1-4]):(0\d|1\d|2\d|3\d|4\d|5\d))*-((0\d|1\d|2[1-4]):(0\d|1\d|2\d|3\d|4\d|5\d))*$/;
            if( !format.test(value) ){
                return "format is not correct";
            }
        },

        formatFactoryNationalId: function(value, attr){
                
            var first = value.slice(0,2);
            if(  value && first && first != 10 ){
                return 'false hast';
            }

            if(  value && value.length != 10 ){
                return 'count';
            }
        },

        parse: function(obj) {
            _.each(this.dateFields, function(field){
                if(obj[field] && obj[field] != ""){
                    var date = App.fromUTC(obj[field]);
                    obj[field] = moment(date).format("YYYY/MM/DD HH:mm")
                }

            });
            return obj;
        },
        
        getFullName: function (item) {
            
            var fullname = '';
            if( item && item.firstname && item.lastname ){
                fullname = item.firstname + ' ' + item.lastname;
            }
            this.set({fullname: fullname},{silent: true});
            return fullname;
        },

        sendVerify: function(opt){

            var self            = this,
                url             = opt.url,
                successCallback = opt.successCallback,
                failCallBack    = opt.failCallBack;

            $.ajax({
                type    : 'post',
                // url     :  + '/api/verify' + url,
                url: url,
            }).done(function( data ) {
                successCallback && successCallback(data);

            }).fail(function(data){
                failCallBack && failCallBack(data);
            });
        },

        removeItem: function(opt){
            
            var self = this,
                successCallback = ( opt && opt.successCallback),
                failCallBack = ( opt && opt.failCallBack);

            var id = this.get('id');
            this.clear();
            this.set({ 'id': id,  'deleted' : '1' }, {silent: true});

            this.save ({},
                {   
                    success: function(model, resp) {
                        successCallback && successCallback();
                    },error: function(){
                        failCallBack && failCallBack();
                    }
                }
            ); 
        },
        username: function(value){
           var en = /^[a-z\d]*$/i;
           if(value && !(en.test(value)  && value.length > 5) )
               return 'has a problem';
       },

       sendAjax: function(opt){
            var url = opt.url,
                data = opt.data,
                successCallback = opt.successCallback,
                failCallBack = opt.failCallBack;

            $.ajax({
                url     : url,
                type    : "POST",
                data    : JSON.stringify(data),
                dataType: 'json',
                contentType: 'application/json',
                success: function(data) {
                    successCallback && successCallback(data);
                },
                error: function(){
                    failCallBack && failCallBack();
                }
            });
       }
    });

    return BaseModel;
});
