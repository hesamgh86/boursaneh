define([

    'core/application',
    'core/model/base',

], function( Application, BaseModel ) {
    var Model = BaseModel.extend({
        name : "news",
        defaults : {
        },
        itemValidation: {
           
        },
        url : function(){
            return Application.URL + "/api/posts" + (this.get("id") ? "/" + this.get("id") : "") ;

        },
    });
    return Model;
});
