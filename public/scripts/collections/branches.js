define([
    
    'core/application',
    'core/collection/base',
    'models/branche'

], function( Application, BaseCollections, BrancheModel ) {

    var Collection = BaseCollections.extend({
        model: BrancheModel,
        name : "Branches",
        url : function(){
            return Application.URL + '/api/branches.json';
        },
    });

    return Collection;
});
