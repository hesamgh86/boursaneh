define([

    'core/application',
    'core/components/base_section',
    'components/branches/views/list',
    'text!components/branches/tpls/main.html',
    
    ], function(Application, BaseSection, ListView, MainTpl) {
        var MainView = BaseSection.extend({
            name  : 'branches',
            events:{
            },
            initialize: function(options) {
                MainView.__super__.initialize.call(this, this.options);
            },
            
            beforeRender: function(){
                this.render();
            },

            afterRender: function(){
                this.createListView();
            },

            getTemplate: function(){
                return _.template(MainTpl);
            },
            
            getListView: function(){
                return new ListView({
                    status : this.status,
                    mode : 'branches',
                });
            },

            afterAppend: function(){},

        });

    return MainView;
});