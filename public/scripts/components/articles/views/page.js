define([
    
    'core/application',
    'core/components/base_page',   
    'core/components/base_map',   
    'collections/articles',  
    'models/article',
    'text!components/articles/tpls/page.html', 
    'components/articles/views/form',    
    'bootbox',
    'lightBox',

    ], function( Application, BasePageView , MapView , Collection, Model, PageTpl, FormView,bootbox  ) {

        var PageView = BasePageView.extend({
            name      : 'articles',
            isNeedMap : false,

            initialize: function() {

                if( this.options && this.options.collection ){
                    this.collection = this.options.collection;
                }
                if( this.options && this.options.model ){
                    this.model = this.options.model;
                }else{
                    this.model = new Model();
                }


                this.collection = new Collection();
                
                if( this.options && this.options.id ){

                    if(  this.options.id.split('-').length > 2 ){
                        this.model.set({ dateStr: this.options.id });
                        this.model.unset('id');
                    }
                }

                PageView.__super__.initialize.call(this, this.options);
            },
            
            getTemplate: function () {
                return _.template(PageTpl);
            },


        });

        return PageView;
    });
